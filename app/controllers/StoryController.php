<?php

class StoryController extends BaseController
{
    protected static $allowedImageExtensions =
        array('gif', 'jpeg', 'jpg', 'png');

    const MAX_IMAGE_SIZE = 1048576; // 1MB

    /**
     * StoryController Constructor
     */
    public function __construct ()
    {
        // Exit if bad csrf token on our form submissions
        $this->beforeFilter('csrf', array('only' => 'share'));
        $this->beforeFilter('csrf', array('only' => 'create'));

        // Exit if any routes match and not logged in
        $this->beforeFilter('auth', array('only' => 'topic'));
        $this->beforeFilter('auth', array('only' => 'create'));
        $this->beforeFilter('auth', array('only' => 'share'));
        $this->beforeFilter('auth', array('only' => 'edit'));
        $this->beforeFilter('auth', array('only' => 'imageUpload'));
        $this->beforeFilter('auth', array('only' => 'imageRemove'));
        $this->beforeFilter('auth', array('only' => 'imageFavorite'));
    }

    /**
     * Log in to or Sign Up for application
     *
     * @param $page string 'login' or 'signup' depending on what page we're on
     * @return Response
     */
    public function login ()
    {
        $page = Input::get('page');

        if (!$page) {
            $page = 'login';
        }

        // If we're logged in, redirect
        if (Auth::check()) {
            return Redirect::to('story/topic');
        }
        
        $settings = Setting::first();
        
        // Determine if this is a first time admin login
        if ($settings->initial_admin_login) {
            return Redirect::to('admin/create');
        }

        $data = array(
            'step' => 1,
            'page' => $page,
            'termsOfUse' => $settings->terms_of_use,
            'privacyPolicy' => $settings->privacy_policy,
            'navbarData' => Utils::getNavbarData(-2),
        );

        return View::make('layouts.story.login')->with('data', $data);
    }

    /**
     * Choose a topic when sharing a story
     *
     * @param $id integer Topic Id that is currently selected
     * @return Response
     */
    public function topic ()
    {
        $topics = Topic::all()->sortBy('order');
        $topicId = Input::get('id');

        $topicsData = array();
        foreach ($topics as $topic) {
            $tmp = array(
                'id' => $topic->id,
                'name' => $topic->name,
                'short_description' => $topic->short_description,
                'long_description' => $topic->long_description,
                'selected' => false,
            );
            if ($topicId != null && ($topicId == $topic->id)) {
                $tmp['selected'] = true;
            }
            $topicsData[] = $tmp;
        }
        if ($topicId == null) {
            $topicsData[0]['selected'] = true;
        }

        $data = array(
            'step' => 2,
            'topicsData' => $topicsData,
            'navbarData' => Utils::getNavbarData(-2),
        );

        return View::make('layouts.story.topic')->with('data', $data);
    }

    /**
     * Present the 'Create' screen to the user
     *
     * @param $topicId integer Currently selected Topic Id
     * @param $storyId integer Currently selected Story Id
     * @return Response
     */
    public function create ()
    {
        $topicId = Input::get('topicId');
        $storyId = Input::get('storyId');

        /*
         * If we have a storyId stored in our session, we haven't finished our
         * story up yet.
         */
        if (Session::has('storyId')) {
            $storyId = Session::get('storyId');

            // Verify this is still valid
            $story = Story::find($storyId);
            if (!$story) {
                // Nope, nuke storyId;
                $storyId = null;
            }
        }

        if (!$storyId) {
            // Verify topic Id valid
            $topic = Topic::find($topicId);
            if (!$topic) {
                return Redirect::to('/story/topic');
            }

            // Generate a storyId & save an empty story
            $story = new Story;
            $story->user_id = Auth::id();
            $story->topic_id = $topicId;
            $story->save();

            $storyId = $story->id;

            $story->short_code = Shortener::convertIntToShortCode($storyId);

            $story->save();

            Session::put('storyId', $storyId);
        }

        $firstName = '';

        $profile = App::make('anvard')->getProfile();
        if ($profile && $profile->firstName) {
            $firstName = $profile->firstName;
        }

        $formData = array(
            'storyId' => $story->id,
            'storyFirstName' => $firstName,
            'storyTitle' => '',
            'storySummary' => '',
            'storyHtml' => '',
            'storyImages' => $story->images,
        );

        $data = array(
            'step' => 3,
            'formUrl' => Config::get('app.url') . '/story/share',
            'editMode' => false,
            'formData' => $formData,
            'navbarData' => Utils::getNavbarData(-2),
        );

        return View::make('layouts.story.storyedit')->with('data', $data);
    }

    /**
     * Validate the input from the story
     *
     * @param int $storyId The story's Id
     * @param string $name First name of the person posting the story
     * @param string $title Title of the story
     * @param string $storyText HTML blob of the actual story
     * @return bool Input valid?
     */
    private function validateStoryInput ($storyId, $name, $title, $storyText)
    {
        if (!strlen($name) || !strlen($title) || !strlen($storyText)) {
            return false;
        }

        if ($storyId <= 0) {
            return false;
        }

        // Make sure this story is ours to modify
        $story = Story::find($storyId);
        if (!$story || $story->user_id != Auth::id()) {
            return false;
        }

        return true;
    }

    /**
     * Present the 'Share your Story' page to the user, as well as creating
     * the actual entry for the story in the database
     *
     * @param $name string Name of person contributing story
     * @param $title string Title of story
     * @param $storyText string Story Html Text
     * @param $storyId integer Story Id of submitted story
     * @return Response
     */
    public function share ()
    {
        $name = Input::get('name');
        $title = Input::get('title');
        $storyText = Input::get('storyText');
        $storyId = Input::get('storyId');

        if (!$this->validateStoryInput($storyId, $name, $title, $storyText)) {
            // Validation should have occurred in the browser.   This is bad.
            return Redirect::to('story/topic');
        }

        // XXX RKJ we need to HtmlPurify the incoming storytext

        /*
         * This story should already exist.  Since we don't support editing of
         * stories (yet), it should be in 'draft' state and we can populate
         * our fields.  We will move it to an 'unpublished' state.  It will
         * be viewable by directly linking to it (via "Share On Social
         * Media"), but will not show up in summaries or be searchable until
         * it is approved by an administrator.
         */
        $story = Story::find($storyId);
        $story->first_name = $name;
        $story->title = $title;
        $story->story_html = $storyText;
        $story->state = 'unpublished';

        $story->save();

        // Nuke our draft Id in memory
        Session::forget('storyId');

        $shareData = array(
            'storyId' => $storyId,
            'storyShortCode' => $story->short_code,
            'storyShortUrl' => Shortener::getShortUrl($story->short_code),
            'storyContent' =>
                View::make('layouts.story.story')->with('storyData',
                    Utils::buildStoryData($story))->render(),
            );

        $data = array(
            'step' => 4,
            'shareData' => $shareData,
            'navbarData' => Utils::getNavbarData(-2),
        );

        return View::make('layouts.story.share')->with('data', $data);
    }

    /**
     * Show a story given a short code
     *
     * @param $shortCode string Short code to look up
     * @return Response
     */
    public function show ($shortCode)
    {
        if (!$shortCode || !Shortener::validateShortCode($shortCode)) {
            App::abort(404);
        }


        $story = Story::where('short_code', '=', $shortCode);

        if (!$story || $story->count() != 1) {
            App::abort(404);
        }

        $story = $story->first();
        if (!$story) {
            return '';
        }

        $storyData = Utils::buildStoryData($story);

        $data = array(
            'storyContent' => View::make('layouts.story.story')
                ->with('storyData', $storyData)->render(),
            'navbarData' => Utils::getNavbarData($story->topic_id),
        );

        return View::make('layouts.story.storyshow')->with('data', $data);
    }

    /**
     * Called by the Javascript image uploader to upload images to the server
     *
     * @param $image FILE Uploaded image information
     * @param $storyId integer Story Id to associate the image with
     * @return Response
     */
    public function imageUpload ()
    {
        if (!Input::hasFile('image')) {
            return Response::json(array(
                'error' => 'Error with the image upload!'
            ), 400);
        }

        $storyId = Input::get('storyId');
        if (!$storyId) {
            return Response::json(array(
                'error' => 'Error with the image upload!'
            ), 400);
        }
        $story = Story::find($storyId);
        if ($story->user_id != Auth::id()) {
            return Response::json(array(
                'error' => 'Error with the image upload!'
            ), 400);
        }

        $image = Input::file('image');
        if (!$image->isValid()) {
            return Response::json(array(
                'error' => 'Error with the image upload!'
            ), 400);
        }

        $extension = $image->getClientOriginalExtension();
        if (!in_array($extension, self::$allowedImageExtensions)) {
            return Response::json(array(
                'error' => 'Invalid image uploaded!'
            ), 400);
        }

        if ($image->getSize() > self::MAX_IMAGE_SIZE) {
            return Response::json(array(
                'error' => 'Image must be less than ' . self::MAX_IMAGE_SIZE
                . ' bytes.'
            ), 400);
        }

        $img = getimagesize($image->getRealPath());
        if (!$img) {
            return Response::json(array(
                'error' => 'Invalid image uploaded!'
            ), 400);
        }

        // See if we have any other images for this story that are favorites
        $favorite = true;
        $favorites = Image::where('story_id', '=', $storyId)
            ->where('favorite', '=', '1')->get();
        if ($favorites && $favorites->count() > 0) {
            $favorite = false;
        }

        $name = ImageDir::storeImage($image);

        $imageEntry = new Image();
        $imageEntry->story_id = $storyId;
        $imageEntry->user_id = Auth::id();
        $imageEntry->filename = $name;
        $imageEntry->favorite = $favorite ? 1 : 0;
        $imageEntry->save();

        return array(
            'fileName' => $name,
            'imageId' => $imageEntry->id,
            'favorite' => $favorite,
        );
    }

    /**
     * Called to remove an image.  Note that ImageDir class checks
     * permissions.
     *
     * @param $imageId integer Image Id to remove
     * @return Response
     */
    public function imageRemove ()
    {
        return ImageDir::removeImage(Input::get('imageId'));
    }

    /**
     * Called to favorite an image.
     *
     * @param $imageId integer Image to favorite
     * @return array('success' => Success?)
     */
    public function imageFavorite ()
    {
        $imageId = Input::get('imageId');

        $success = false;
        if ($imageId) {
            $success = ImageDir::favoriteImage($imageId);
        }

        return array('success' => $success);
    }

    /**
     * Called to display and/or actually edit a story.
     *
     * @param $storyId integer Story Id to edit
     * @param $name string Name of person associated with story
     * @param $title string Title of story
     * @param $storyText string Story Html Text
     * @param $editMode string If set, this is an actual edit
     * @return Response
     */
    public function edit ()
    {
        $storyId = Input::get('storyId');
        $name = Input::get('name');
        $storyText = Input::get('storyText');
        $title = Input::get('title');
        $editMode = Input::get('editMode');

        if (!$storyId) {
            App::abort(404);
        }

        /*
         * Criteria for editing: story must be mine, and be not deleted
         */
        $story = Story::where('id', '=', $storyId)
            ->where('user_id', '=', Auth::id())
            ->where(function ($query) {
                $query->where('state', '=', 'published')
                    ->orWhere('state', '=', 'unpublished')
                    ->orWhere('state', '=', 'draft');
            })
            ->get()
            ->first();
        if (!$story || $story->user_id != Auth::id()) {
            App::abort(404);
        }

        // Lets see if this is an incoming edit.
        if ($editMode) {
            // Yup, lets validate the data
            if (!$this->validateStoryInput($storyId, $name, $title, $storyText)) {
                // Validation should have occurred in the browser.   This is bad.
                return Redirect::to('embed/dashboard');
            }

            // Data is good, lets update it.
            $story->first_name = $name;
            $story->title = $title;
            $story->story_html = $storyText;
            $story->state = 'unpublished';

            $story->save();

            // Might be nice to put a 'success' message...

            return Redirect::to('embed/dashboard');
        }

        // We are okay to edit this.
        $formData = array(
            'storyId' => $story->id,
            'storyFirstName' => $story->first_name,
            'storyTitle' => $story->title,
            'storyHtml' => $story->story_html,
            'storyImages' => $story->images,
        );

        $data = array(
            'formUrl' => Config::get('app.url') . '/story/edit',
            'editMode' => true,
            'formData' => $formData,
            'navbarData' => Utils::getNavbarData($story->topic_id),
        );

        return View::make('layouts.story.storyedit')->with('data', $data);
    }

}

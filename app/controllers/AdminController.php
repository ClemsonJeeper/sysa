<?php

class AdminController extends BaseController
{
    const PAGE_DASHBOARD = 0;
    const PAGE_STORIES = 10;
    const PAGE_TOPICS = 20;
    const PAGE_USERS = 30;
    const PAGE_SETTINGS = 40;

    /**
     * AdminController constructor.  Note that ALL routes in this controller
     * are protected and only executable by a logged in administrator.
     */
    public function __construct ()
    {
        // Exit if any routes match and not logged in
        $this->beforeFilter('auth.admin', array('only' => 'dashboard'));
        $this->beforeFilter('auth.admin', array('only' => 'stories'));
        $this->beforeFilter('auth.admin', array('only' => 'topics'));
        $this->beforeFilter('auth.admin', array('only' => 'users'));
        $this->beforeFilter('auth.admin', array('only' => 'settings'));
    }

    /**
     * Admin Dashboard
     * @return Response
     */
    public function dashboard ()
    {
        // Calculate out some stats for us
        $statsData = array();

        $statsData[] = array(
            'caption' => 'Published Stories',
            'value' => number_format(Story::where('state', 'published')->count()),
        );

        $statsData[] = array(
            'caption' => 'Unpublished Stories',
            'value' => number_format(Story::where('state', 'unpublished')->count()),
        );

        $statsData[] = array(
            'caption' => 'Verified Users',
            'value' => number_format(User::where('validated', 1)->count()),
        );

        $statsData[] = array(
            'caption' => 'Unverified Users',
            'value' => number_format(User::where('validated', 0)->count()),
        );

        $statsData[] = array(
            'caption' => 'Hosted Images',
            'value' => number_format(Image::all()->count()),
        );

        $statsData[] = array(
            'caption' => 'Topics',
            'value' => number_format(Topic::all()->count()),
        );

        $data = array(
            'navbarData' => Utils::getNavbarData(-3),
            'statsData' => $statsData,
            'adminNavBarData' => Utils::getAdminNavbarData(self::PAGE_DASHBOARD),
        );

        return View::make('layouts.admin.dashboard')->with('data', $data);
    }

    /**
     * Administer stories
     *
     * @param storyIds string Comma delimited list of story Ids to work on
     * @param delete string If set, this is a delete operation
     * @param state string State to filter the stories by
     * @param title string Title to filter the stories by
     * @return Response
     */
    public function stories ()
    {
        $storyIds = Input::get('storyIds');
        $delete = Input::get('delete');
        $state = Input::get('state');
        $title = Input::get('title');

        if ($storyIds) {
            // We are approving or deleting
            $state = 'published';
            if ($delete) {
                $state = 'deleted';
            }

            $storyIds = preg_split('/,/', $storyIds);
            if (count($storyIds)) {
                Story::whereIn('id', $storyIds)
                    ->update(array('state' => $state));
            }

            return array(
                'success' => true
            );
        }

        // Get the # of stories needing approval
        $stories = Story::where('state', 'unpublished')
            ->get();
        $unapprovedStories = $stories->count();

        // Get all stories with filter
        $stories = Story::where(function ($query) use ($state, $title) {
            if ($state && ($state == 'unpublished') || ($state == 'published')
                || ($state == 'deleted'))
            {
                $query->where('state', $state);
            }

            if ($title && strlen($title)) {
                $query->where('title', 'like', '%' . $title . '%');
            }
        })->orderBy('created_at', 'DESC');

        $data = array(
            'navbarData' => Utils::getNavbarData(-3),
            'storyData' => $stories->paginate(10),
            'unapprovedStories' => $unapprovedStories,
            'adminNavBarData' => Utils::getAdminNavbarData(self::PAGE_STORIES),
        );

        return View::make('layouts.admin.approvestories')->with('data', $data);
    }

    /**
     * Administer topics
     *
     * @param $topicId string Individual topic Id to work on
     * @param $name string Topic name
     * @param $shortDescription string Topic short description
     * @param $longDescription string Topic long description
     * @param $TopicIds string Comma delimited list of topic Ids to work on
     * @param $delete string If set, this is a delete operation
     * @return Response
     */
    public function topics ()
    {
        $topicId = Input::get('topicId');
        $name = Input::get('name');
        $shortDescription = Input::get('shortDescription');
        $longDescription = Input::get('longDescription');
        $order = Input::get('order');
        $topicIds = Input::get('topicIds');
        $delete = Input::get('delete');

        if ($delete) {
            // Deleting topics... eek!
            $topicIds = preg_split('/,/', $topicIds);

            foreach ($topicIds as $topicId) {
                // First, nuke the stories associated with this topic
                Story::where('topic_id', $topicId)->delete();

                // Now nuke the topic
                Topic::where('id', $topicId)->delete();
            }

            return array(
                'success' => true,
            );
        }

        if ($name || $shortDescription || $longDescription || $order) {
            // We are adding or editing, check values

            $success = false;
            $title = $message = $field = '';
            if (!strlen($name)) {
                $title = 'Error';
                $message = 'Please enter a name for this topic.';
                $field = 'name';
            } else if (!strlen($shortDescription)) {
                $title = 'Error';
                $message = 'Please enter a short description for this topic.';
                $field = 'shortDescription';
            } else if (!strlen($longDescription)) {
                $title = 'Error';
                $message = 'Please enter a long description for this topic.';
                $field = 'longDescription';
            } else if ($order <= 0) {
                $title = 'Error';
                $message = 'Please enter a non negative display order for this topic.';
                $field = 'order';
            } else {
                // We ok
                $success = true;

                $topic = null;
                if ($topicId != -1) {
                    // This is an edit
                    $topic = Topic::find($topicId);
                    if (!$topic) {
                        $title = 'Error';
                        $message = 'There was an error editing that topic.';
                        $field = 'name';
                        $success = false;
                    }
                } else {
                    // This is an add
                    $topic = new Topic;
                }

                if ($topic) {
                    $topic->name = $name;
                    $topic->short_description = $shortDescription;
                    $topic->long_description = $longDescription;
                    $topic->order = $order;
                    $topic->save();
                }
            }
            return array(
                'success' => $success,
                'title' => $title,
                'message' => $message,
                'field' => $field,
            );
        }

        $topics = Topic::orderBy('order')
            ->get();

        $data = array(
            'navbarData' => Utils::getNavbarData(-3),
            'topicData' => $topics,
            'adminNavBarData' => Utils::getAdminNavbarData(self::PAGE_TOPICS),
        );

        return View::make('layouts.admin.topics')->with('data', $data);
    }

    /**
     * Administer Users
     *
     * @param $userIds string Comma delimited list of user Ids to work on
     * @param $delete string If set, this is a delete operation
     * @param $validate string If set, this is a validate operation
     * @return Response
     */
    public function users ()
    {
        $userIds = Input::get('userIds');
        $delete = Input::get('delete');
        $validate = Input::get('validate');

        // Deleting users... eek!
        if ($delete) {
            $userIds = preg_split('/,/', $userIds);

            foreach ($userIds as $userId) {
                if ($userId == Auth::id()) {
                    // Duh, sanity check. Don't delete yourself!
                    continue;
                }

                // First, nuke the stories associated with this user
                Story::where('user_id', $userId)->delete();

                // First, nuke any associated social media profiles
                $profiles = Profile::where('user_id', $userId)
                    ->delete();

                // Now nuke the user
                User::where('id', $userId)
                    ->delete();
            }

            return array(
                'success' => true,
            );
        }

        // Validating users
        if ($validate) {
            $userIds = preg_split('/,/', $userIds);
            if (count($userIds)) {
                User::whereIn('id', $userIds)
                    ->update(array('validated' => 1));
            }

            return array(
                'success' => true
            );
        }

        $data = array(
            'navbarData' => Utils::getNavbarData(-3),
            'users' => User::paginate(10),
            'adminNavBarData' => Utils::getAdminNavbarData(self::PAGE_USERS),
        );

        return View::make('layouts.admin.users')->with('data', $data);
    }

    /**
     * Administer Settings
     *
     * @param $termsOfUse string Terms of use to set
     * @param $privacyPolicy string Privacy policy to set
     * @return Response
     */
    public function settings ()
    {
        $termsOfUse = Input::get('termsOfUse');
        $privacyPolicy = Input::get('privacyPolicy');

        $setting = Setting::first();

        if ($termsOfUse) {
            // Settting settings

            $success = false;
            $title = $message = $field = '';
            if (!strlen($termsOfUse)) {
                $title = 'Error';
                $message = 'You must enter something for the terms of use.';
                $field = 'termsOfUse';
            } else if (!strlen($privacyPolicy)) {
                $title = 'Error';
                $message = 'You must enter something for the privacy policy.';
                $field = 'privacyPolicy';
            } else {
                $success = true;

                $setting->terms_of_use = $termsOfUse;
                $setting->save();
            }

            return array(
                'success' => $success,
                'title' => $title,
                'message' => $message,
                'field' => $field,
            );
        }

        $data = array(
            'navbarData' => Utils::getNavbarData(-3),
            'settingsData' => $setting,
            'adminNavBarData' => Utils::getAdminNavbarData(self::PAGE_SETTINGS),
        );

        return View::make('layouts.admin.settings')->with('data', $data);
    }

    /**
     * Create the initial admin account
     *
     * @param $create boolean Creating an account?
     * @param $email string Email address of admin account
     * @param $password string Password to use for admin account
     * @param $confirmPassword string Confirm password
     * @return Response
     */
    public function create ()
    {
        $create = Input::get('create');
        $email = Input::get('email');
        $password = Input::get('password');
        $confirmPassword = Input::get('confirmPassword');

        $settings = Setting::first();
        if (!$settings->initial_admin_login) {
            App::abort(404);
        }

        if ($create) {
            $success = false;
            $message = $title = $field = '';

            $validatePassword = Utils::validatePassword($password, $confirmPassword);
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $title = 'Invalid Email';
                $message = 'Please enter a valid Email address.';
                $field = 'email';
            } else if (!$validatePassword['success']) {
                $title = $validatePassword['title'];
                $message = $validatePassword['message'];
                $field = $validatePassword['field'];
            } else {
                // We're okay, create the account
                $success = true;

                $user = new User;
                $user->email = $email;
                $user->password = Hash::make($password);
                $user->admin = 1;
                $user->validated = 1;
                $user->save();

                // Make it so we can't do this again
                DB::table('settings')->update(array('initial_admin_login' => 0));

                $title = 'Admin Account Created';
                $message = 'The administrator account has been created.  You can now log in with this account.';
            }

            return array(
                'success' => $success,
                'title' => $title,
                'message' => $message,
                'field' => $field,
            );
        }

        $data = array(
            'navbarData' => Utils::getNavbarData(-3),
            'adminNavBarData' => Utils::getAdminNavbarData(self::PAGE_SETTINGS),
        );

        return View::make('layouts.admin.create')->with('data', $data);
    }

}

?>

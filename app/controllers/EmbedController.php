<?php

class EmbedController extends BaseController
{
    // Number of results to return per page on summary screens
    const PER_PAGE = 3;

    /**
     * EmbedController constructor
     */
    public function __construct ()
    {
        // Exit if any routes match and not logged in
        $this->beforeFilter('auth', array('only' => 'dashboard'));
    }

    /**
     * Splash screen for users that are embedded
     *
     * @param $a string If set, return as AJAXP
     * @param $logout string If set, log the user out
     * @return Response
     */
    public function splash ()
    {
        $ajaxp = Input::get('a');
        $logout = Input::get('logout');
        if ($logout) {
            App::make('hybridauth')->logoutAllProviders();
            Auth::logout();
        }

        // Pull out 3 latest published stories for the top bar
        $stories = Story::where('state', '=', 'published')
            ->take(3)
            ->orderBy('created_at', 'desc')
            ->get();

        $topStories = array();
        foreach ($stories as $story) {
            $topStory = array(
                'storyUrl' => $story->getStoryUrl(),
                'storyTitle' => ($story->title),
            );

            $image = $story->favoriteImage();
            if ($image) {
                $topStory['imageUrl'] =
                    ImageDir::getImageThumbUrl($image->filename);
            }

            array_push($topStories, $topStory);
        }

        if (count($topStories) < 3) {
            while (count($topStories) < 3) {
                $topStories[] = array();
            }
        }

        // Pull out our topics & the most recent published story in the topic
        $topics = Topic::all()->sortBy('order');

        $topicData = array();
        foreach ($topics as $topic) {
            $out = array(
                'topicId' => $topic->id,
                'topicName' => $topic->name,
                'topicDescription' => $topic->short_description,
            );
            $topicStory = Story::where('state', '=', 'published')
                ->where('topic_id', '=', $topic->id)
                ->take(1)
                ->orderBy('created_at', 'desc')
                ->get()
                ->first();
            if ($topicStory) {
                $out += array(
                    'topicStoryUrl' => $topicStory->getStoryUrl(),
                    'topicStoryTitle' => $topicStory->title,
                );

                $image = $topicStory->favoriteImage();
                if ($image) {
                    $out['topicStoryImageUrl'] =
                        ImageDir::getImageThumbUrl($image->filename);
                }
            }
            $topicData[] = $out;
        }

        $data = array(
            'topStories' => $topStories,
            'topicData' => $topicData,
            'navbarData' => Utils::getNavbarData(0),
        );

        $view = View::make('layouts.embed.splash')->with('data', $data);

        if ($ajaxp) {
            $response = Response::make(
                'rS(' . json_encode(array('html' => $view->render())) . ')', 200);
            $response->header('Content-Type', 'application/javascript');
            return $response;
        } else {
            return $view;
        }
    }

    /**
     * Show all stories in a given topic
     *
     * @param $topicId integer Topic Id to view
     * @return Response
     */
    public function topic ($topicId)
    {
        if ($topicId < 0) {
            App::abort(404);
        }

        $topic = Topic::find($topicId);
        if (!$topic) {
            App::abort(404);
        }

        // Pull out 3 latest published stories from this topic for the top bar
        $stories = Story::where('state', '=', 'published')
            ->where('topic_id', '=', $topicId)
            ->take(3)
            ->orderBy('created_at', 'desc')
            ->get();

        $topStories = array();
        foreach ($stories as $story) {
            $topStory = array(
                'storyUrl' => $story->getStoryUrl(),
                'storyTitle' => ($story->title),
            );

            $image = $story->favoriteImage();
            if ($image) {
                $topStory['imageUrl'] =
                    ImageDir::getImageThumbUrl($image->filename);
            }

            array_push($topStories, $topStory);
        }

        if (count($topStories) < 3) {
            while (count($topStories) < 3) {
                $topStories[] = array();
            }
        }

        $data = array(
            'topicId' => $topicId,
            'topicName' => $topic->name,
            'topStories' => $topStories,
            'navbarData' => Utils::getNavbarData($topicId),
        );

        return View::make('layouts.embed.topic')->with('data', $data);
    }

    /**
     * Get the story data for a given topic Id with an offset
     *
     * @param $topicId integer Topic Id to search
     * @param $start integer Offset of story to begin with
     * @return array Array of storyData to pass to the view
     */
    private function getStoryData ($topicId, $start)
    {
        $stories = Story::where('state', '=', 'published')
            ->where('topic_id', '=', $topicId)
            ->where('id', '<=', $start)
            ->take(self::PER_PAGE)
            ->orderBy('created_at', 'desc')
            ->get();

        if ($stories->count() == 0) {
            return array();
        }

        $storyData = array();
        foreach ($stories as $story) {
            $storyData[] = Utils::buildStoryData($story);
        }

        return $storyData;
    }

    /**
     * This function returns raw story HTML to be used by the ajax background
     * loader.
     *
     * @param $topicId integer Topic Id to pull stories from
     * @param $start integer Offset to pull stories from
     * @return Response
     */
    public function rawStories ()
    {
        $topicId = Input::get('topicId');
        $start = Input::get('start');

        if ($topicId <= 0) {
            return '';
        }

        if (!$start) {
            $start = 4294967295; // maxint
        }

        $data = array(
            'storyData' => $this->getStoryData($topicId, $start),
            'showStatus' => false,
        );

        $lastEntry = end($data['storyData']);
        if ($lastEntry) {
            $data['start'] = $lastEntry['story']->id - 1;
        }

        return View::make('layouts.embed.rawstory')->with('data', $data);
    }

    /**
     * Show the user dashboard where they can edit & delete stories
     *
     * @param $delete string Story Id to attempt to delete
     * @return Response
     */
    public function dashboard ()
    {
        $delete = Input::get('delete');

        $socialProfile =  App::make('anvard')->getProfile();

        if ($delete) {
            // Attempt a delete before showing the dashboard
            $story = Story::find($delete);
            if ($story->user_id == Auth::id()) {
                $story->state = 'deleted';
                $story->save();
            }
        }

        // Get all our stories that are published/unpublished
        $stories = Story::join('topics', 'stories.topic_id', '=', 'topics.id')
            ->select('stories.*', 'topics.name')
            ->where('stories.user_id', '=', Auth::id())
            ->where(function ($query) {
                $query->where('stories.state', '=', 'published')
                    ->orWhere('stories.state', '=', 'unpublished');
            })
            ->orderBy('topics.order')
            ->orderBy('stories.created_at', 'desc')
            ->get();

        $storyHtml = '';

        // Build up our array to render the stories
        $currentTopic = -1;
        $data = array(
            'topicName' => '',
            'showEditButtons' => true,
            'storyData' => array(),
            'showStatus' => true,
        );

        foreach ($stories as $story) {
            if ($currentTopic != $story->topic_id) {
                if (count($data['storyData'])) {
                    $storyHtml .=
                        View::make('layouts.embed.rawstory')
                          ->with('data', $data)->render();
                }

                $data = array(
                    'topicName' => $story->topic->name,
                    'showEditButtons' => true,
                    'storyData' => array(),
                    'showStatus' => true,
                );
                $currentTopic = $story->topic_id;
            }
            $data['storyData'][] = Utils::buildStoryData($story);
        }

        $storyHtml .= View::make('layouts.embed.rawstory')
            ->with('data', $data)->render();

        $data = array(
            'navbarData' => Utils::getNavbarData(-1),
            'storyHtml' => $storyHtml,

        );

        return View::make('layouts.embed.dashboard')->with('data', $data);
    }
}

<?php

class RemindersController extends Controller
{

    /**
     * Display the password reminder view.
     *
     * @return Response
     */
    public function getRemind ()
    {
        return View::make('password.remind');
    }

    /**
     * Handle a POST request to remind a user of their password.
     *
     * @return Response
     */
    public function postRemind ()
    {
        $success = false;
        switch ($response = Password::remind(Input::only('email'), function ($message) {
            $message->subject('"Share Your Story" Password Reminder');
        })) {
        case Password::INVALID_USER:
            $message = Lang::get($response);
            break;
        case Password::REMINDER_SENT:
            $success = true;
            $message = Lang::get($response);
            break;
        }

        return array(
            'success' => $success,
            'message' => $message,
        );
    }

    /**
     * Display the password reset view for the given token.
     *
     * @param  string  $token
     * @return Response
     */
    public function getReset ($token = null)
    {
        if (is_null($token)) {
            App::abort(404);
        }

        $data = array(
            'token' => $token,
            'navbarData' => Utils::getNavbarData(-3),
        );

        return View::make('layouts.login.reset')->with('data', $data);
    }

    /**
     * Handle a POST request to reset a user's password.
     *
     * @return Response
     */
    public function postReset ()
    {
        $credentials = Input::only(
            'email', 'password', 'password_confirmation', 'token'
        );

        $success = false;

        $response = Password::reset($credentials, function($user, $password) {
            $user->password = Hash::make($password);

            $success = true;

            $user->save();
        });

        switch ($response) {
        case Password::INVALID_PASSWORD:
        case Password::INVALID_TOKEN:
        case Password::INVALID_USER:
            $message = Lang::get($response);
            break;
        case Password::PASSWORD_RESET:
            $success = true;
            $message = 'Your password has been reset.  You can now log in with your new password.';
            break;
        }

        return array(
            'success' => $success,
            'message' => $message,
        );
    }
}

?>

<?php

class LoginController extends BaseController
{
    const MAX_EMAIL_LENGTH = 128;

    /**
     * LoginController constructor
     */
    public function __construct ()
    {
        // Only authenticated users
        $this->beforeFilter('auth', array('only' => 'changeEmail'));
        $this->beforeFilter('auth', array('only' => 'changePassword'));
                $success = true;
    }

    /**
     * Login or create an account via email/password
     *
     * @param create string Create an account?
     * @param email string Email address to use for the username
     * @param password string Clear-text password to use for the login
     * @param confirmPassword string Clear-text password to use to verify password
     * @param termsConfirm string 'true' if terms have been confirmed by
     *   checkbox
     *
     * @return json array(
     *                 success => boolean whether or not successful
     *                 title => title of the dialog box
     *                 message => message of the dialog box
     *                 field => field name to focus (if any)
     *                 create => we are in create user mode?
     *              )
     */
    public function login ()
    {
        $email = Input::get('email');
        $password = Input::get('password');
        $confirmPassword = Input::get('confirmPassword');
        $termsConfirm = Input::get('termsConfirm');
        $create = (Input::get('create') === 'true');

        // Sanity check
        $success = false;
        $message = $title = $field = '';

        if (empty($email) || strlen($email) > self::MAX_EMAIL_LENGTH ||
            !filter_var($email, FILTER_VALIDATE_EMAIL))
        {
            $title = 'Invalid Email';
            $message = 'Please enter a valid Email address.';
            $field = 'email';
        } else if ($create) {
            // Creating a new user, check some things.
            $validatePassword = Utils::validatePassword($password, $confirmPassword);

            if (!$validatePassword['success']) {
                $title = $validatePassword['title'];
                $message = $validatePassword['message'];
                $field = $validatePassword['field'];
            } else if ($termsConfirm !== 'true') {
                $title = 'Terms and Conditions';
                $message = 'You must agree to the terms and conditions before continuing.';
                $field = 'termsconfirm';
            } else {
                $user = User::where('email', '=', $email)->first();
                if ($user) {
                    $title = 'Invalid Email';
                    $message = 'That Email address is already registered.  If you forgot your password, please use the "Forgot Password" link on the login screen.';
                    $field = 'email';
                } else {
                    // Now create our new, valid User & send out email about validation
                    $success = true;
                    $user = new User;
                    $user->email = $email;
                    $user->password = Hash::make($password);
                    $user->validation_hash = sha1(time() . $email);
                    $user->save();

                    $title = 'User Account Created';
                    $message = 'Thank you for signing up to share your story.  You have been sent an e-mail with instructions on how to activate your account.';

                    Mail::send(array(
                        'emails.auth.validatehtml',
                        'emails.auth.validatetext',
                    ), array('activateUrl' => Config::get('app.url') . '/login/validate?email=' . $email . '&hash=' . $user->validation_hash), function ($message) use ($email) {
                        $message->to($email)->subject('Please validate your "Share Your Story" account');
                    });
                }
            }
        } else {
            // Logging in an existing user, check some things
            if (strlen($password) <= 0) {
                $title = 'Invalid Password';
                $message = 'Please enter a valid password.';
                $field = 'password';
            } else {
                // Logging in a user and everythings okay.

                if (Auth::attempt(array('email' => $email, 'password' => $password))) {
                    // Check to see if account has been validated
                    $user = User::where('email', $email)->first();
                    if ($user->validated) {
                        Auth::loginUsingId($user->id);
                        $title = 'Success';
                        $message = 'You have been successfully logged into "Share Your Story".';
                        $field = '';
                        $success = true;
                    } else {
                        $title = 'Cannot Login';
                        $message = 'You have not yet confirmed your Email address.  Please check your registered Email address for an Email from "Share Your Story" detailing how to activate your account.';
                        $field = '';
                    }
                } else {
                    $title = 'Incorrect Login';
                    $message = 'Invalid Email or password.  If you forgot your password, please follow the "Forgotten Password" link.';
                    $field = 'email';
                }
            }
        }

        return array(
            'success' => $success,
            'title' => $title,
            'message' => $message,
            'field' => $field,
            'create' => $create,
        );
    }

    /**
     * Login via Social Network
     *
     * @param provider string Provider to use (eg, Facebook/Twitter)
     * @return Redirect to the correct location if logged in or not
     */
    private function socialLogin ($provider)
    {
        $profile = App::make('anvard')->attemptAuthentication($provider,
            App::make('hybridauth'));
        if ($profile) {
            Auth::loginUsingId($profile->user->id);
            return Redirect::to('story/topic');
        } else {
            return Redirect::to('story/login');
        }
    }

    /**
     * Logout of Share Your Story
     *
     * @return Redirect to login page
     */
    public function logout ()
    {
        App::make('hybridauth')->logoutAllProviders();
        Auth::logout();
        return Redirect::to('story/login');
    }

    /**
     * Login via Facebook API
     *
     * @return Redirect to correct location if logged in or not
     */
    public function facebookLogin ()
    {
        return $this->socialLogin('Facebook');
    }

    /**
     * Login via Twitter API
     *
     * @return Redirect to correct location if logged in or not
     */
    public function twitterLogin ()
    {
        return $this->socialLogin('Twitter');
    }

    /**
     * Login via Google API
     *
     * @return Redirect to correct location if logged in or not
     */
    public function googleLogin ()
    {
        return $this->socialLogin('Google');
    }

    /**
     * Validate email/password account
     *
     * @param $email string Email to use to verify
     * @param $hash string hash sent to user to verify
     * @return View Account validated or 404 error
     */
    public function validate ()
    {
        $email = Input::get('email');
        $hash = Input::get('hash');

        if (!strlen($email) || !strlen($hash)) {
            App::abort(404);
        }

        $user = User::where('email', '=', $email)
            ->where('validation_hash', '=', $hash)->first();

        if (!$user) {
            App::abort(404);
        }

        $user->validated = 1;
        $user->save();

        $data = array(
            'email' => $user->email,
            'navbarData' => Utils::getNavbarData(-2),
        );

        return View::make('layouts.login.accountvalidated')->with('data', $data);
    }

    /**
     * Called by ajax to change the Email address of a user
     *
     * @param $email string Email address to request reset
     * @param $confirmEmail string Confirmed Email address to request reset
     * @return Response
     */
    public function changeEmail ()
    {
        $email = Input::get('email');
        $confirmEmail = Input::get('confirmEmail');

        $user = User::find(Auth::id());
        if (!$user) {
            App::abort(404);
        }

        $success = false;
        $message = $title = $field = '';
        if (empty($email) || strlen($email) > self::MAX_EMAIL_LENGTH ||
            !filter_var($email, FILTER_VALIDATE_EMAIL))
        {
            $title = 'Invalid Email';
            $message = 'Please enter a valid Email address.';
            $field = 'email';
        } else if ($email != $confirmEmail) {
            $title = 'Invalid Email';
            $message = 'Please confirm the Email address is correct and matches.';
            $field = 'email';
        } else if ($email == $user->email) {
            $title = 'Invalid Email';
            $message = 'The Email you entered is the same as your current Email.';
            $field = 'email';
        } else {
            // We good.  Update the user record & send off new verification Email
            $user->email_change = $email;
            $user->email_change_hash = sha1(time() . $email);
            $user->save();

            Mail::send('emails.auth.validateemailchange', array('activateUrl' => Config::get('app.url') . '/login/validateEmailChange?email=' . $user->email_change . '&hash=' . $user->email_change_hash), function ($message) use ($email) {
                $message->to($email)->subject('Please validate your "Share Your Story" Email address change');
            });

            $title = 'Successful';
            $message = 'Please check the Email address you just entered for instructions on how to continue the Email change process.';
            $field = '';
            $success = true;
        }


        return array(
            'success' => $success,
            'title' => $title,
            'message' => $message,
            'field' => $field,
        );
    }

    /**
     * Called when validating the Email change
     *
     * @param $email string Email address to validate
     * @param $hash string Hash sent in Email to validate change
     * @return Response
     */
    public function validateEmailChange ()
    {
        $email = Input::get('email');
        $hash = Input::get('hash');

        if (!strlen($email) || !strlen($hash)) {
            App::abort(404);
        }

        $user = User::where('email_change', $email)
            ->where('email_change_hash', $hash)
            ->first();

        if (!$user) {
            App::abort(404);
        }

        $user->email = $user->email_change;
        $user->email_change = $user->email_change_hash = '';
        $user->save();

        $data = array(
            'email' => $user->email,
            'navbarData' => Utils::getNavbarData(-2),
        );

        return View::make('layouts.login.emailchanged')->with('data', $data);
    }

    /**
     * Called when requesting a password change
     *
     * @param $currentPassword string Current password
     * @param $password string New password
     * @param $confirmPassword string Confirm new password
     * @return Response
     */
    public function changePassword ()
    {
        $currentPassword = Input::get('currentPassword');
        $password = Input::get('password');
        $confirmPassword = Input::get('confirmPassword');

        $user = User::find(Auth::id());
        if (!$user) {
            App::abort(404);
        }

        $validatePassword = Utils::validatePassword($password, $confirmPassword);
        $title = $message = $field = '';
        $success = false;
            if (!$currentPassword) {
            $title = 'Invalid password';
            $message = 'Please enter your correct current password.';
            $field = 'currentpassword';
        } else if (!$validatePassword['success']) {
            $title = $validatePassword['title'];
            $message = $validatePassword['message'];
            $field = $validatePassword['field'];
        } else if (!Hash::check($currentPassword, $user->password)) {
            $title = 'Invalid password';
            $message = 'Please enter your correct current password.';
            $field = 'currentpassword';
        } else {
            $success = true;

            // Save new password
            $user->password = Hash::make($password);
            $user->save();

            $title = 'Successful';
            $message = 'Password change was successful.';
        }

        return array(
            'success' => $success,
            'title' => $title,
            'message' => $message,
            'field' => $field,
        );
    }

}

<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface
{
    use UserTrait, RemindableTrait;

    protected $table = 'users';
    protected $hidden = array('password', 'remember_token');

    public function profiles ()
    {
        return $this->hasMany('Profile');
    }

    public function stories ()
    {
        return $this->hasMany('Story');
    }

    public function getCreatedAtShort ()
    {
        $createdAt = new DateTime($this->created_at);
        return $createdAt->format('M jS, Y \a\t g:i A');
    }
}

?>

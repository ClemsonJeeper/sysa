<?php

class Story extends Eloquent
{
    protected $table = 'stories';

    public function user ()
    {
        return $this->belongsTo('User');
    }

    public function images ()
    {
        return $this->hasMany('Image');
    }

    public function favoriteImage ()
    {
        return $this->images()
            ->where('favorite', '=', 1)
            ->first();
    }

    public function getStoryUrl ()
    {
        return Shortener::getShortUrl($this->short_code);
    }

    public function topic ()
    {
        return $this->belongsTo('Topic');
    }

    public function getCreatedAtShort ()
    { 
        $createdAt = new DateTime($this->created_at);
        return $createdAt->format('M jS, Y \a\t g:i A');
    }
}

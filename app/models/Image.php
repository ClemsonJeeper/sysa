<?php

class Image extends Eloquent
{
    public $timestamps = false;

    public function story ()
    {
        return $this->belongsTo('Story');
    }

    public function getUrl ()
    {
        return ImageDir::getImageUrl($this->filename);
    }

    public function getThumbUrl ()
    {
        return ImageDir::getImageThumbUrl($this->filename);
    }
}

?>

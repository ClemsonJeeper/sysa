<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <h2>"Share Your Story" Email Change</h2>
    <div>
       <p>In order to complete the Email change process, please confirm your new Email address by clicking on the following link.</p>
       <p><a href="{{ $activateUrl }}">Confirm My Email Adress</a></p>
    </div>
  </body>
</html>

<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <h2>"Share Your Story" Account Creation</h2>
    <div>
       <p>Thank you for registering to create an account to share your story about smoking. In order to activate your account, please click on the following link.</p>
       <p><a href="{{ $activateUrl }}">Activate My Account</a></p>
       <p>After you have activated your account, you can continue to share your story about smoking.</p>
    </div>
  </body>
</html>

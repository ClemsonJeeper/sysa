"Share Your Story" Account Creation

Thank you for registering to create an account to share your story about smoking. In order to activate your account, please click on the following link.

{{ $activateUrl }}

After you have activated your account, you can continue to share your story about smoking.

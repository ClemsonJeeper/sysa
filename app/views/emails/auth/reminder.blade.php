<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="utf-8">
  </head>
  <body>
    <h2>"Share Your Story" Password Reset</h2>
    <div>
      <p>Someone has requested that your password on <a href="{{ Config::get('app.url') }}">Share Your Story</a> be reset.</p>
      <p>To reset your password, please follow this link: <a href="{{ URL::to('password/reset', array($token)) }}">Reset My Password</a>.</p>
      <p>This link will expire in {{ Config::get('auth.reminder.expire', 60) }} minutes.</p>
    </div>
  </body>
</html>

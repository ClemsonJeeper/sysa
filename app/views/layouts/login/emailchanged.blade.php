@extends('layouts.master')

@section('header')
@stop

@section('content')
<div class="well">
  <h5><strong>You have successfully changed your Email address to: <strong>{{ $data['email'] }}</strong></h5>
  <h5><a href="{{ Config::get('app.url') . '/embed/dashboard' }}">My Dashboard</a></h5>
</div>
@stop

@extends('layouts.master')

@section('content')
<div class="container-fluid">
<div class="well">
  <h4>To complete the password reset process, please enter the following information:</h4>
  {{ Form::open(array('url' => Config::get('app.url') . '/password/reset', 'method' => 'post')) }}
    {{ Form::hidden('token', $data['token']) }}
    <div class="form-group">
      <label for="email">Email Address</label>
      {{ Form::text('email', '', array('class' => 'form-control', 'placeholder' => 'Enter Email address')) }}
    </div>
    <div class="form-group">
      <label for="password">New Password</label>
      {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Enter new password', 'id' => 'password')) }}
    </div>
    <div class="form-group">
      <label for="password_confirmation">Confirm New Password</label>
      {{ Form::password('password_confirmation', array('class' => 'form-control', 'placeholder' => 'Confirm new password')) }}
    </div>
  {{ Form::close() }}
  <button id="reset" class="btn btn-primary">Reset Password</button>
</div>

<script type="text/javascript">
$(function () {
    $('button#reset').click(function () {
        $.ajax('{{ Config::get('app.url') . '/password/reset' }}', {
            type: 'post',
            data: {
                email: $('input[name=email]').val(),
                password: $('input[name=password]').val(),
                password_confirmation: $('input[name=password_confirmation]').val(),
                token: $('input[name=token]').val(),
                _token: $('input[name=_token]').val()
            },
            success: function (data, textStatus, jqXHR) {
                if (!data.success) {
                    popupDialog('Error', data.message);
                } else {
                    popupDialog('Success', data.message, function () {
                        window.location.href = '{{ Config::get('app.url') . '/story/login' }}';
                    });
                }
            }
        });
    });
});

</script>

@stop

@extends('layouts.master')

@section('header')
@stop

@section('content')
<div class="well">
  <h5><strong>You have successfully validated your "Share Your Story" account.  You can now share your stories about how smoking has affected your life.</strong></h5>
  <h5>You may now <a href="{{ Config::get('app.url') . '/story/login' }}">Log Into Your Account</a>.</h5>
</div>
@stop

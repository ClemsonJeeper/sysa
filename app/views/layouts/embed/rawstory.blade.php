@if (isset($data['topicName']))
  @if (count($data['storyData'])) 
    <hr class="fadebar"/>
    <h1>My stories about {{ $data['topicName'] }}</h1>
  @else
    <h3 class="text-center">You have not shared any stories yet, why not <a href="{{ Config::get('app.url') . '/story/topic' }}">Share Your First Story</a>?</h3>
  @endif
@endif
@foreach ($data['storyData'] as $story)
  <hr class="fadebar"/>
  <div class="storybody container">
    <div class="row">
      <div class="col-md-8">
        @if (isset($story['profilePhotoUrl']))
          <img class="pull-left profilephoto" style="margin-right: 10px" src="{{ $story['profilePhotoUrl'] }}"/>
        @else
          <img data-src="holder.js/64x64/text:X" style="border: 1px solid #ccc"/>
        @endif       
        <strong>{{ $story['story']->first_name }}<br/>
        <a id="storylink" href="{{ $story['storyUrl'] }}">{{ $story['story']->title }}</a></strong>
        @if ($story['createdAt'])
        <div class="topicstorydateblock">Posted: <b>{{ $story['createdAt'] or '' }}</b></div>
        @endif
      </div>
      <div class="col-md-2 text-right">
        @if ($data['showStatus'])
          @if ($story['story']->state == 'published')
            <div class="btn btn-success">Published</div>
          @elseif ($story['story']->state == 'unpublished')
            <div class="btn btn-warning">Unpublished</div>
          @elseif ($story['story']->state == 'draft')
            <div class="btn btn-warning">Draft</div>
          @elseif ($story['story']->state == 'deleted')
            <div class="btn btn-danger">Deleted</div>
          @endif
        @endif
      </div>
      <div class="col-md-2 text-right">
        @if (isset($data['showEditButtons']) && $data['showEditButtons'])
          <a href="{{ Config::get('app.url') . '/story/edit?storyId=' . $story['storyId'] }}" class="btn editbuttons btn-default">Edit Story</a><br/>
          <button type="button" class="btn editbuttons btn-default" onClick="deleteStory(this)" data-story-delete-url="{{ Config::get('app.url') . '/embed/dashboard?delete=' . $story['storyId'] }}">Delete Story</a><br/>
        @endif
      </div>
    </div>
    <div id="storysummary" style="display: none">{{ $story['storySummary'] }}</div>
    <div class="story">
      {{ $story['story']->story_html }}
    </div>
    <div class="shareblock">
      <div class="share">Share:</div>
      <ul>
        <li class="facebookicon" onClick="shareStory('facebook', this)"/>
      </ul>
    </div>
    <div class="fb-comments" data-width="100%" data-href="{{ $story['storyUrl'] }}" data-numposts="5" data-colorscheme="light"/>
  </div>
@endforeach
@if (isset($data['start']))
  <div class="container">
    <div class="col-md-12 text-center">
      <button data-start="{{ $data['start'] }}" type="button" class="loadmore btn btn-default btn-lg">Show More Stories</button>
    </div>
  </div>
@endif

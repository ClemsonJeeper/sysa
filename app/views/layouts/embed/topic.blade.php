@extends('layouts.master')

@section('header')
{{ HTML::script(Config::get('app.url') . '/resources/js/jquery.min.js') }}
@stop

@section('content')
<div class="container">
  <div class="row sharetop">
    <div class="col-md-9">
      <h1>Stories about {{ $data['topicName'] }}</h1>
    </div>
    <div class="col-md-3">
      <a href="{{ Config::get('app.url') . '/story/topic' }}">
        <img src="{{ Config::get('app.url') . '/resources/images/shareyourstory.png' }}"/>
      </a>
    </div>
  </div>
  <div class="row">
    @for ($i = 0; $i < 3; $i++)
      <div class="col-md-4 text-center images">
        @if (isset($data['topStories'][$i]['storyUrl']))
          <a href="{{ $data['topStories'][$i]['storyUrl'] }}" class="thumbnail">
            @if (isset($data['topStories'][$i]['imageUrl']))
              <img src="{{ $data['topStories'][$i]['imageUrl'] }}">
            @else
              <img data-src="holder.js/150x150/text:No Thumbnail">
            @endif
              <div class="caption">
                <p>{{ $data['topStories'][$i]['storyTitle'] }}</p>
              </div>
            </img>
          </a>
        @else
          <a href="javascript:undefined" class="thumbnail">
            <img data-src="holder.js/150x150/text:No Thumbnail">
              <div class="caption">
                <p>No Stories Yet</p>
              </div>
            </img>
          </a>
        @endif
      </div>
    @endfor
  </div>
</div>
<br/>
<div class="embedtopicstories"/>

<script type="text/javascript">
function loadMoreStories (start)
{
    $.ajax('{{ Config::get('app.url') . '/embed/rawstories' }}', {
        data: {
            topicId: {{ $data['topicId'] }},
            start: start
        },
        success: function (data, textStatus, jqXHR) {
            var ets = $('div.embedtopicstories');
            ets.append(data);
            ets.find('button.loadmore').click(function () {
                loadMoreStories($(this).attr('data-start'));
                this.remove();
            });
            FB.XFBML.parse();
        }
    });
}

$(function () {
    $.ajaxSetup({ cache: true });
    $.getScript('//connect.facebook.net/en_US/all.js', function() {
        FB.init({
            appId: '{{ Config::get('anvard::hybridauth')['providers']['Facebook']['keys']['id'] }}',
        });
        loadMoreStories(0);
    });
});

</script>

@stop

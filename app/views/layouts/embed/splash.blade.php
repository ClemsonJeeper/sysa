@extends('layouts.master')

@section('content')
<div class="container">
  <div class="row sharetop">
    <div class="col-md-9"></div>
    <div class="col-md-3">
      <a href="{{ Config::get('app.url') . '/story/topic' }}">
        <img src="{{ Config::get('app.url') . '/resources/images/shareyourstory.png' }}"/>
      </a>
    </div>
  </div>
  <div class="row">
    @for ($i = 0; $i < 3; $i++)
      <div class="col-md-4 text-center images">
        @if (isset($data['topStories'][$i]['storyUrl']))
          <a href="{{ $data['topStories'][$i]['storyUrl'] }}" class="thumbnail">
            @if (isset($data['topStories'][$i]['imageUrl'])) 
              <img src="{{ $data['topStories'][$i]['imageUrl'] }}">
            @else
              <img data-src="holder.js/150x150/text:No Thumbnail">
            @endif
              <div class="caption">
                <p>{{ $data['topStories'][$i]['storyTitle'] }}</p>
              </div>
            </img>
          </a>
        @endif
      </div>
    @endfor
  </div>
</div>
<h2 class="text-center"><strong>Browse personal stories -- and share yours!</strong></h2>
<br/><br/>
<h3>Real stories about ...</h3>
<div class="container">
  @for ($i = 0; $i < count($data['topicData']); $i++)
    @if (($i % 2) == 0)
      <div class="row text-center">
    @endif
        <div class="col-md-1"></div>
        <div class="col-md-4">
          <div class="text-left">
            <strong><h3><a href="{{ Config::get('app.url') . '/embed/topic/' . $data['topicData'][$i]['topicId'] }}">{{ $data['topicData'][$i]['topicName'] }}</a></h3></strong>
            <small>{{ $data['topicData'][$i]['topicDescription'] }}</small>
            <div class="text-center">
              @if (isset($data['topicData'][$i]['topicStoryUrl']))
                <a href="{{ $data['topicData'][$i]['topicStoryUrl'] }}" class="thumbnail">
                  @if (isset($data['topicData'][$i]['topicStoryImageUrl']))
                    <img src="{{ $data['topicData'][$i]['topicStoryImageUrl'] }}">
                  @else
                    <img data-src="holder.js/150x150/text:No Thumbnail">
                  @endif
                    <div class="caption">
                      <p>{{ $data['topicData'][$i]['topicStoryTitle'] }}</p>
                    </div>
                  </img>
                </a>
              @else
                <a href="javascript:undefined" class="thumbnail">
                  <img data-src="holder.js/150x150/text:No Thumbnail">
                    <div class="caption">
                      <p>No Stories Yet</p>
                    </div>
                  </img>
                </a>
              @endif              
            </div>
          </div>
        </div>
        <div class="col-md-1"></div>
    @if (($i %2) == 1)
      </div>
    @endif     
  @endfor
</div>
@stop

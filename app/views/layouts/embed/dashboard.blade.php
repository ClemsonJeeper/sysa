@extends('layouts.master')

@section('header')
@stop

@section('content')
<div class="container">
  <div class="well">
    <h4><strong>Your Information</strong></h4><br/>
    <small><a id="changeemail" href="javascript:undefined">Change Email Address</a></small><br/>
    <small><a id="changepassword" href="javascript:undefined">Change Password</a></small><br/>
  </div>
  {{ $data['storyHtml'] or '' }}
</div>

<div id="dialog-email-form" title="Change Email Address" style="display: none">
  <p>All form fields are required.</p>
    <div class="form-group">
      <label class="control-label" for="email">New Email Address</label>
      <input type="text" name="email" id="email" value="" class="form-control"/>
    </div>
    <div class="form-group">
      <label class="control-label" for="confirmemail">Confirm New Email Address</label>
      <input type="text" name="confirmemail" id="confirmemail" value="" class="form-control"/>
    </div>
</div>

<div id="dialog-password-form" title="Change Password" style="display: none">
  <p>All form fields are required.</p>
    <div class="form-group">
      <label class="control-label" for="currentpassword">Current Password</label>
      <input type="password" name="currentpassword" id="currentpassword" value="" class="form-control"/>
    </div>
    <div class="form-group">
      <label class="control-label" for="password">New Password</label>
      <input type="password" name="password" id="password" value="" class="form-control"/>
    </div>
    <div class="form-group">
      <label class="control-label" for="confirmpassword">Confirm New Password</label>
      <input type="password" name="confirmpassword" id="confirmpassword" value="" class="form-control"/>
    </div>
</div>

<script type="text/javascript">
$(function () {
    $.ajaxSetup({ cache: true });
    $.getScript('//connect.facebook.net/en_US/all.js', function () {
        FB.init({
            appId: '{{ Config::get('anvard::hybridauth')['providers']['Facebook']['keys']['id'] }}',
        });
        FB.XFBML.parse();
    });

    $('a#changeemail').click(function () {
        $('div#dialog-email-form input#email').val();
        $('div#dialog-email-form input#confirmemail').val();
        $('div#dialog-email-form').dialog({
            autoOpen: true,
            height: 325,
            width: 350,
            modal: true,
            buttons: {
                'Submit': function () {
                    var changeEmailDialog = this;
                    $.ajax('{{ Config::get('app.url') . '/login/changeEmail' }}', {
                        data: {
                            email: $('input#email').val(),
                            confirmEmail: $('input#confirmemail').val(),
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (!data.success) {
                                popupDialog(data.title, data.message, function () {
                                    $('input#' + data.field).focus()
                                });
                            } else {
                                // User was created successfully,
                                popupDialog(data.title, data.message, function () {
                                    $(changeEmailDialog).dialog('close');
                                });
                            }
                        }
                    });

                },
                'Cancel': function () {
                    $(this).dialog('close');
                }
            }
        });
    });

    $('a#changepassword').click(function () {
        $('div#dialog-password-form input#password').val();
        $('div#dialog-password-form input#confirmpassword').val();
        $('div#dialog-password-form').dialog({
            autoOpen: true,
            height: 385,
            width: 350,
            modal: true,
            buttons: {
                'Submit': function () {
                    var changePasswordDialog = this;
                    $.ajax('{{ Config::get('app.url') . '/login/changePassword' }}', {
                        data: {
                            currentPassword: $('input#currentpassword').val(),
                            password: $('input#password').val(),
                            confirmPassword: $('input#confirmpassword').val()
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (!data.success) {
                                popupDialog(data.title, data.message, function () {
                                    $('input#' + data.field).focus()
                                });
                            } else {
                                // User was created successfully,
                                popupDialog(data.title, data.message, function () {
                                    $(changePasswordDialog).dialog('close');
                                });
                            }
                        }
                    });

                },
                'Cancel': function () {
                    $(this).dialog('close');
                }
            }
        });
    });
});

function deleteStory (me)
{
    confirmDialog('Confirm Delete', 'Are you sure you want to delete this story?', function (del) {
        if (del) {
            if ($(me).attr('data-story-delete-url')) {
                window.location.href = $(me).attr('data-story-delete-url');
            }
        }
    });
}
</script>
@stop

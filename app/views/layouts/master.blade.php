<!DOCTYPE html>
<html>
  <head>
    {{ HTML::script(Config::get('app.url') . '/resources/js/jquery.min.js') }}
    {{ HTML::script(Config::get('app.url') . '/resources/js/jquery-ui.js') }}

    {{ HTML::style(Config::get('app.url') . '/resources/css/jquery-ui.css') }}
    {{ HTML::style(Config::get('app.url') . '/resources/css/bootstrap.min.css') }}
    {{ HTML::style(Config::get('app.url') . '/resources/css/bootstrap-theme.min.css') }}
    {{ HTML::style(Config::get('app.url') . '/resources/css/sysa.css') }}
    {{ HTML::script(Config::get('app.url') . '/resources/js/holder.min.js') }}
    @section('header')
    @show
  </head>
  <body>
    <div id="popupdialog">
      <div id="message"></div>
    </div>
    <div class="storyblock grayborder">
      <div class="container-fluid">
        <nav class="navbar navbar-default" role="navigation">
          <div class="container-fluid">
            <div id="navbar" class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                @foreach ($data['navbarData']['left'] as $navbar)
                  @if ($navbar['selected']) 
                    <li class="active">
                  @else
                    <li>
                  @endif
                  <a href="{{ $navbar['url'] }}">{{ $navbar['name'] }}</a>
                  </li>
                @endforeach
              </ul>
              <ul class="nav navbar-nav navbar-right">
                @foreach ($data['navbarData']['right'] as $navbar)
                  @if ($navbar['selected']) 
                    <li class="active">
                  @else
                    <li>
                  @endif
                  <a href="{{ $navbar['url'] }}">{{ $navbar['name'] }}</a>
                  </li>
                @endforeach
              </ul>
            </div>
          </div>
        </nav>
      </div>
      @section('content')
      @show
    </div>
    <div class="container text-center"><small>&copy; 2014 "Share Your Story", etc other legal mumbo-jumbo in footer.</small></div>
    <script type="text/javascript">
function shareStory (network, me)
{
    var storyBlock = $(me).parent().parent().parent();
    if (network == 'facebook') {
        FB.ui({
            method: 'feed',
            display: 'dialog',
            link: storyBlock.find('a#storylink').attr('href'),
            name: storyBlock.find('a#storylink').html(),
            caption: storyBlock.find('div#storysummary').html()
        }, function (response) {
            if (response && !response.error_code) {
                popupDialog('Success', 'Thank you for sharing your story on Facebook!');
            }
        });
    }
}
function popupDialog (title, message, callback)
{
    $('div#popupdialog div#message').text(message);
    $('div#popupdialog').dialog({
        title: title,
        resizable: false,
        modal: true,
        //position: { my: 'top', at: 'top+25%' },
        buttons: {
            'OK': function () {
                $(this).dialog('close');
                if (callback !== undefined) {
                    callback();
                }
            }
        }
    });
}
function confirmDialog (title, message, callback)
{
    $('div#popupdialog div#message').text(message);
    $('div#popupdialog').dialog({
        title: title,
        resizable: false,
        modal: true,
        position: { my: 'top', at: 'top+25%' },
        buttons: {
            'Yes': function () {
                $(this).dialog('close');
                if (callback !== undefined) {
                    callback(true);
                }
            },
            'No': function () {
                $(this).dialog('close');
                if (callback !== undefined) {
                    callback(false);
                }
            }
        }
    });
}
    </script>
  </body>
</html>

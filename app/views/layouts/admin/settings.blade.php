@extends('layouts.master')

@section('header')
{{ HTML::script('/resources/js/redactor/redactor.min.js') }}
{{ HTML::script('/resources/js/redactor/fontcolor.js') }}
{{ HTML::script('/resources/js/redactor/video.js') }}
{{ HTML::style('/resources/css/redactor.css') }}
@stop

@section('content')
@include('layouts.admin.navbar')
<div class="container-fluid">
  <div class="well">
    <h4>General Settings</h4>
    <div class="form-group">
      <label class="control-label" for="termsOfUse">Terms Of Use</label>
      <textarea name="termsOfUse" id="termsOfUse" class="form-control">{{ $data['settingsData']->terms_of_use }}</textarea>
    </div>
    <div class="form-group">
      <label class="control-label" for="privacyPolicy">Privacy Policy</label>
      <textarea name="privacyPolicy" id="privacyPolicy" class="form-control">{{ $data['settingsData']->privacy_policy }}</textarea>
    </div>
     <button id="save" type="button" class="btn btn-success">Save Changes</button>
     <button id="cancel" type="button" class="btn btn-danger">Cancel</button>
  </div>
</div>

<script type="text/javascript">
$(function () {
    $('button#cancel').click(function () {
        location.reload();
    });

    $('button#save').click(function () {
        $.ajax('{{ Config::get('app.url') . '/admin/settings' }}', {
            type: 'post',
            data: {
                termsOfUse: $('textarea#termsOfUse').val(),
                privacyPolicy: $('textarea#privacyPolicy').val()
            },
            success: function (data, textStatus, jqXHR) {
                if (!data.success) {
                    popupDialog(data.title, data.message, function () {
                        $('input#' + data.field).focus()
                    });
                } else {
                    location.reload();
                }
            }
        });
    });

    $('textarea#termsOfUse').redactor({
        minHeight: 220,
        maxHeight: 220,
        plugins: [ 'fontcolor', 'video' ]
    });
    
    $('textarea#privacyPolicy').redactor({
        minHeight: 220,
        maxHeight: 220,
        plugins: [ 'fontcolor', 'video' ]
    });
});
</script>
@stop

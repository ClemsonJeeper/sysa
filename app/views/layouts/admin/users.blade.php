@extends('layouts.master')

@section('content')
@include('layouts.admin.navbar')
<div class="container-fluid">
  <div class="well">
    @if (count($data['users']))
      <h4>Users</h4>
      {{ $data['users']->links() }}
      <table class="table table-bordered table-striped">
        <tr>
          <th><input type="checkbox" class="checkall"></input></th>
          <th class="col-sm-7">Email</th>
          <th class="col-sm-3">Created On</th>
          <th class="col-sm-1">Validated?</th>
          <th class="col-sm-1">Admin?</th>
        </tr>
        @foreach ($data['users'] as $user)
          <tr>
            <td><input type="checkbox" class="checkuser" id="{{ $user->id }}"></input></td>
            <td><a id="edituser" href="javascript:undefined">{{ $user->email }}</a></td>
            <td>{{ $user->getCreatedAtShort() }}</td>
            <td id="validated">{{ $user->validated ? 'Yes' : 'No' }}</td>
            <td id="admin">{{ $user->admin ? 'Yes' : 'No' }} </td>
          </tr>
        @endforeach
       </table>
       <button id="validate" type="button" class="btn btn-primary">Validate Checked Users</button>
       <button id="delete" type="button" class="btn btn-danger">Delete Checked Users</button>
    @else
      <h4>There are no users.</h4>
    @endif
  </div>
</div>

<div id="dialog-user-form" title="Edit User" style="display: none">
  <div class="form-group">
    <label class="control-label" for="email">Email</label>
    <input type="text" name="email" id="email" value="" class="form-control"/>
  </div>
  <div class="form-group">
    <label class="control-label" for="validated">Validated?</label>
    <input type="checkbox" name="validated" id="validated"/>
  </div>
  <div class="form-group">
    <label class="control-label" for="admin">Admin?</label>
    <input type="checkbox" name="admin" id="admin"/>
  </div>
</div>

<script type="text/javascript">
$(function () {
    $('input.checkall').click(function () {
        var check = $(this).prop('checked');
        $('.checkuser').each(function () {
            $(this).prop('checked', check);
        });
    });

    $('button#delete').click(function () {
        var checked = $('input.checkuser:checked');
        if (!checked.length) {
            popupDialog('Error', 'You must select at least one user to delete.');
            return;
        }

        confirmDialog('Delete?', 'Are you sure you wish to delete these users?  All stories written by these users will be permanently deleted!', function (result) {
            if (result) {
                var ids = [];
                checked.each(function () {
                    ids.push($(this).attr('id'));
                });
                $.ajax('{{ Config::get('app.url') . '/admin/users' }}', {
                    data: {
                        delete: true,
                        userIds: ids.join(',')
                    },
                    success: function (data, textStatus, jqXHR) {
                        location.reload();
                    }
                });
            }
        });
    });
    
    $('button#validate').click(function () {
        var checked = $('input.checkuser:checked');
        if (!checked.length) {
            popupDialog('Error', 'You must select at least one user to validate.');
            return;
        }

        confirmDialog('Validate?', 'Are you sure you wish to validate these users?', function (result) {
            if (result) {
                var ids = [];
                checked.each(function () {
                    ids.push($(this).attr('id'));
                });
                $.ajax('{{ Config::get('app.url') . '/admin/users' }}', {
                    data: {
                        validate: true,
                        userIds: ids.join(',')
                    },
                    success: function (data, textStatus, jqXHR) {
                        location.reload();
                    }
                });
            }
        });
    });

    $('a#edituser').click(function () {
        $(this).parent().parent().find('input.checkuser').attr('id')
        $('div#dialog-user-form input#email').val($(this).text());
        $('div#dialog-user-form input#validated').prop('checked', ($(this).parent().parent().find('td#validated').text().trim() == 'Yes'));
        $('div#dialog-user-form input#admin').prop('checked', ($(this).parent().parent().find('td#admin').text().trim() == 'Yes'));
        $('div#dialog-user-form').dialog({
            autoOpen: true,
            height: 350,
            width: 350,
            modal: true,
            buttons: {
                'Save': function () {
                    var userDialog = this;
                    $.ajax('{{ Config::get('app.url') . '/admin/users' }}', {
                        type: 'post',
                        data: {
                            topicId: topicId,
                            name: $('input#name').val(),
                            shortDescription: $('input#shortDescription').val(),
                            longDescription: $('input#longDescription').val(),
                            order: $('input#order').val()
                        },
                        success: function (data, textStatus, jqXHR) {
                            if (!data.success) {
                                popupDialog(data.title, data.message, function () {
                                    $('input#' + data.field).focus()
                                });
                            } else {
                                location.reload();
                            }
                        }
                    });

                },
                'Cancel': function () {
                    $(this).dialog('close');
                }
            }
        });

    });

    $('input.checkuser').prop('checked', false);
});
</script>
@stop

@extends('layouts.master')

@section('header')
@stop

@section('content')
@include('layouts.admin.navbar')
<div class="container-fluid">
  <div class="well">
    <h4>Current "Share Your Story" Statistics:</h4><br/>
    <dl class="dl-horizontal">
      @foreach ($data['statsData'] as $stat)
        <dt>{{ $stat['caption'] }}</dt>
        <dd>{{ $stat['value'] }}</dd>
      @endforeach
    </dl>  
  </div>
</div>
@stop

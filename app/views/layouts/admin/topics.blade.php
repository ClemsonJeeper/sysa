@extends('layouts.master')

@section('header')
@stop

@section('content')
@include('layouts.admin.navbar')
<div class="container-fluid">
  <div class="well">
    @if (count($data['topicData']))
      <h4>Topics</h4>
      <table class="table table-bordered table-striped">
        <tr>
          <th><input type="checkbox" class="checkall"></input></th>
          <th class="col-sm-3">Name</th>
          <th class="col-sm-3">Short Description</th>
          <th class="col-sm-5">Long Description</th>
          <th class="col-sm-1">Display Order</th>
        </tr>
        @foreach ($data['topicData'] as $topic)
          <tr>
            <td><input type="checkbox" class="checktopic" id="{{ $topic->id }}"></input></td>
            <td><a id="edittopic" href="javascript:undefined">{{ $topic->name }}</a></td>
            <td id="shortDescription">{{ $topic->short_description }}</td>
            <td id="longDescription">{{ $topic->long_description }}</td>
            <td id="order">{{ $topic->order }} </td>
          </tr>
        @endforeach
       </table>
       <button id="add" type="button" class="btn btn-primary">Add New Topic</button>
       <button id="delete" type="button" class="btn btn-danger">Delete Checked Topics</button>
    @else
      <h4>There are no topics.</h4>
    @endif
  </div>
</div>

<div id="dialog-topic-form" title="" style="display: none">
  <p>All form fields are required.</p>
  <div class="form-group">
    <label class="control-label" for="name">Topic Name</label>
    <input type="text" name="name" id="name" value="" class="form-control"/>
  </div>
  <div class="form-group">
    <label class="control-label" for="shortDescription">Short Description</label>
    <input type="text" name="shortDescription" id="shortDescription" value="" class="form-control"/>
  </div>
  <div class="form-group">
    <label class="control-label" for="longDescription">Long Description</label>
    <input type="text" name="longDescription" id="longDescription" value="" class="form-control"/>
  </div>
  <div class="form-group">
    <label class="control-label" for="order">Display Order</label>
    <input type="text" name="order" id="order" value="" class="form-control"/>
  </div>
</div>

<script type="text/javascript">

function showDialog (edit)
{
    var topicId = edit ? edit.topicId : -1;
    $('div#dialog-topic-form').attr('title', edit ? 'Edit Topic' : 'Add New Topic');
    $('div#dialog-topic-form input#name').val(edit ? edit.name : '');
    $('div#dialog-topic-form input#shortDescription').val(edit ? edit.shortDescription : '');
    $('div#dialog-topic-form input#longDescription').val(edit ? edit.longDescription : '');
    $('div#dialog-topic-form input#order').val(edit ? edit.order : '');
    $('div#dialog-topic-form').dialog({
        autoOpen: true,
        height: 500,
        width: 650,
        modal: true,
        buttons: {
            'Save': function () {
                var topicDialog = this;
                $.ajax('{{ Config::get('app.url') . '/admin/topics' }}', {
                    type: 'post',
                    data: {
                        topicId: topicId,
                        name: $('input#name').val(),
                        shortDescription: $('input#shortDescription').val(),
                        longDescription: $('input#longDescription').val(),
                        order: $('input#order').val()
                    },
                    success: function (data, textStatus, jqXHR) {
                        if (!data.success) {
                            popupDialog(data.title, data.message, function () {
                                $('input#' + data.field).focus()
                            });
                        } else {
                            location.reload();
                        }
                    }
                });

            },
            'Cancel': function () {
                $(this).dialog('close');
            }
        }
    });
}

$(function () {
    $('input.checkall').click(function () {
        var check = $(this).prop('checked');
        $('.checktopic').each(function () {
            $(this).prop('checked', check);
        });
    });

    $('button#delete').click(function () {
        var checked = $('input.checktopic:checked');
        if (!checked.length) {
            popupDialog('Error', 'You must select at least one topic to delete.');
            return;
        }

        confirmDialog('Delete?', 'Are you sure you wish to delete these topics?  THIS IS NOT RECOMMENDED!  THIS WILL PERMANENTLY DELETE ALL STORIES ASSOCIATED WITH THIS TOPIC AND IS NOT REVERSIBLE!!', function (result) {
            if (result) {
                confirmDialog('Are you REALLY sure?', 'Are you absolutely sure?  Again, this process is not reversible!!', function (result) {
                    if (result) {
                        var ids = [];
                        checked.each(function () {
                            ids.push($(this).attr('id'));
                        });
                        $.ajax('{{ Config::get('app.url') . '/admin/topics' }}', {
                            data: {
                                delete: true,
                                topicIds: ids.join(',')
                            },
                            success: function (data, textStatus, jqXHR) {
                                location.reload();
                            }
                        });
                    }
                });
            }
        });
    });
    
    $('button#add').click(function () {
        showDialog();
    });

    $('a#edittopic').click(function () {
        showDialog({
            topicId: $(this).parent().parent().find('input.checktopic').attr('id'),
            name: $(this).text(),
            shortDescription: $(this).parent().parent().find('td#shortDescription').text(),
            longDescription: $(this).parent().parent().find('td#longDescription').text(),
            order: $(this).parent().parent().find('td#order').text()
        });
    });

    $('input.checktopic').prop('checked', false);
});
</script>
@stop

@extends('layouts.master')

@section('header')
@stop

@section('content')
@include('layouts.admin.navbar')
<div class="container-fluid">
  <div class="well">
    <h4>Stories</h4>
    @if (count($data['storyData']))
      <h5>There @if ($data['unapprovedStories'] == 1) is @else are @endif {{ $data['unapprovedStories'] }} unpublished @if ($data['unapprovedStories'] == 1) story @else stories @endif awaiting approval.</h5>
      {{ $data['storyData']->links() }}
      <table class="table table-bordered table-striped">
        <tr>
          <th><input type="checkbox" class="checkall"></input></th>
          <th class="col-sm-4">Title</th>
          <th class="col-sm-3">User</th>
          <th class="col-sm-3">Created At</th>
          <th class="col-sm-2">State</th>
        </tr>
        @foreach ($data['storyData'] as $story)
          <tr>
            <td><input type="checkbox" class="checkstory" id="{{ $story->id }}"></input></td>
            <td><a target="_new" href="{{ $story->getStoryUrl() }}">{{ $story->title }}</a></td>
            <td>{{ $story->user->email }}</td>
            <td>{{ $story->getCreatedAtShort() }}</td>
            <td>{{ $story->state }}</td>
          </tr>
        @endforeach
      </table>
    @else
      <h4>There are no stories matching this filter.</h4>
    @endif
    <div class="well">
      <h5>Filter results by...</h5>
      <form class="form-horizontal" role="form">
        <div class="form-group">
          <label for="state" class="col-sm-2 control-label">State</label>
          <div class="col-sm-3">
            {{ Form::select('state', array('empty' => '', 'deleted' => 'deleted', 'unpublished' => 'unpublished', 'published' => 'published'), Input::get('state') ? Input::get('state') : 'empty', array('class' => 'form-control', 'id' => 'state')) }}
          </div>
        </div>
        <div class="form-group">
          <label for="title" class="col-sm-2 control-label">Title</label>
          <div class="col-sm-3">
            {{ Form::text('title', Input::get('title') ? Input::get('title') : '', array('id' => 'title', 'class' => 'form-control')) }}
          </div>
        </div>
        <div class="form-group text-right">
          <div class="col-sm-5">
            <div id="resetfilter" class="btn btn-default">Reset</div>
            <div id="filter" class="btn btn-default">Filter</div>
          </div>
        </div>
      </form>
    </div>
    <button id="approve" type="button" class="btn btn-success">Approve Checked Stories</button>
    <button id="delete" type="button" class="btn btn-danger">Delete Checked Stories</button>
  </div>
</div>
<script type="text/javascript">
$(function () {
    $('input.checkall').click(function () {
        var check = $(this).prop('checked');
        $('.checkstory').each(function () {
            $(this).prop('checked', check);
        });
    });

    $('button#delete').click(function () {
        var checked = $('input.checkstory:checked');
        if (!checked.length) {
            popupDialog('Error', 'You must select at least one story to delete.');
            return;
        }

        confirmDialog('Delete?', 'Are you sure you wish to delete these stories?', function (result) {
            if (result) {
                var ids = [];
                checked.each(function () {
                    ids.push($(this).attr('id'));
                });
                $.ajax('{{ Config::get('app.url') . '/admin/stories' }}', {
                    data: {
                        delete: true,
                        storyIds: ids.join(',')
                    },
                    success: function (data, textStatus, jqXHR) {
                        location.reload();
                    }
                });
            }
        });
    });

    $('button#approve').click(function () {
        var checked = $('input.checkstory:checked');
        if (!checked.length) {
            popupDialog('Error', 'You must select at least one story to approve.');
            return;
        }

        confirmDialog('Delete?', 'Are you sure you wish to approve these stories?', function (result) {
            if (result) {
                var ids = [];
                checked.each(function () {
                    ids.push($(this).attr('id'));
                });
                $.ajax('{{ Config::get('app.url') . '/admin/stories' }}', {
                    data: {
                        storyIds: ids.join(',')
                    },
                    success: function (data, textStatus, jqXHR) {
                        location.reload();
                    }
                });
            }
        });
    });

    $('div#filter').click(function () {
        window.location.href = '{{ Config::get('app.url') . '/admin/stories?' }}'
            + 'state=' + $('select#state option:selected').val()
            + '&title=' + $('input#title').val();
    });

    $('div#resetfilter').click(function () {
        $('select#state').val('empty');
        $('input#title').val('');
    });

    $('input.checkstory').prop('checked', false);
});
</script>
@stop

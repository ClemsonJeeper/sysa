@extends('layouts.master')

@section('content')
<div class="container-fluid">
  <div class="well">
    <h4>Create Administrator Account</h4>
    <div class="form-group">
      <label for="email">Email Address</label>
      <input type="email" class="form-control" id="email" placeholder="Enter Email address"/>
    </div>
    <div class="form-group">
      <label for="password">Password</label>
      <input type="password" class="form-control" id="password" placeholder="Enter your password"/>
    </div>
    <div class="form-group">
      <label for="confirmPassword">Confirm Password</label>
      <input type="password" class="form-control" id="confirmPassword" placeholder="Confirm your password"/>
    </div>
    <button id="create" class="btn btn-primary">Create Admin Account</button>
  </div>
</div>

<script type="text/javascript">
$(function () {
    $('button#create').click(function () {
        $.ajax('{{ Config::get('app.url') . '/admin/create' }}', {
            data: {
                create: true,
                email: $('input#email').val(),
                password: $('input#password').val(),
                confirmPassword: $('input#confirmPassword').val(),
            },
            success: function (data, textStatus, jqXHR) {
              console.log(data);
                if (!data.success) {
                    popupDialog(data.title, data.message, function () {
                        $('input#' + data.field).focus()
                    });
                } else {
                    // Admin user was created successfully
                    popupDialog(data.title, data.message, function () {
                        window.location.href = '{{ Config::get('app.url') . '/story/login' }}';
                    });
                }
            }
        });
    });
});
</script>
@stop

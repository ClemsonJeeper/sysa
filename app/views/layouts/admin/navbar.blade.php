<div style="margin: 20px">
  <ul class="nav nav-pills">
    @foreach ($data['adminNavBarData'] as $navbar)
      <li role="presentation" @if ($navbar['selected'])  class="active" @endif>
        <a href="{{ $navbar['url'] }}">{{ $navbar['name'] }}</a>
      </li>
    @endforeach
  </ul>
</div>

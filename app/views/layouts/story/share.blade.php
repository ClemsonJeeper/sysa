@extends('layouts.master')

@section('content')
@include('layouts.story.progress')
<div id="bodyblock">
  <div id="stepcontent"><img src="/resources/images/4.big.png"/><div id="steptext">Share Your Story!</div></div>
  <div style="font-size: 12px"><p>Congratulations!  You can now share your story!</p></div>
  <div style="font-size: 11px"><p>Your story has not yet been published publically on our site, but you can share a direct link to it on your social media sites.  You will be notified when your submission has been successfully approved and added to our site.</p></div>
  <div id="indentblock">
  {{ $data['shareData']['storyContent'] }}
  </div>
</div>
@stop

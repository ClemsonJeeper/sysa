{{ HTML::script('/resources/galleria/galleria-1.4.2.min.js') }}
<meta property="og:title" content="{{ $storyData['story']->title or '' }}"/>
<meta property="og:url" content="{{ $storyData['storyUrl'] or '' }}"/>
<meta property="og:site_name" content="Share Your Story"/>
<div class="storyblock grayborder">
  <div class="container topicstories">
    <div class="col-md-6">
      @if (isset($storyData['profilePhotoUrl']))
        <img class="pull-left profilephoto" style="margin-right: 10px" src="{{ $storyData['profilePhotoUrl'] }}"/>
      @endif
      <h2><strong>{{ $storyData['story']->first_name }}</strong></h2>
    </div>
    <div class="col-md-6 text-right">
      <a href="{{ Config::get('app.url') . '/story/topic' }}">
        <img src="{{ Config::get('app.url') . '/resources/images/shareyourstory.png' }}"/>
      </a>
    </div>
  </div>
  <div class="titleblock">{{ $storyData['story']->title or '' }}</div>
  @if ($storyData['createdAt'])
  <div class="dateblock">Posted: <strong>{{ $storyData['createdAt'] or '' }}</strong></div>
  @endif
  @if ($storyData['storyTopic'])
  <div class="dateblock">Posted under: <a href="{{ Config::get('app.url') . '/embed/topic/' . $storyData['story']->topic_id }}"><strong>{{ $storyData['storyTopic'] or '' }}</strong></a></div>
  @endif
  <hr class="fadebar"/>
  <div class="storycontentblock">{{ $storyData['story']->story_html or '' }}</div>
  @if (count($storyData['imageData']) > 0)
  <div style="width: 100%">
  <div id="imagegalleryblock" class="imagegalleryblock">
    @foreach ($storyData['imageData'] as $image)
      <a href="{{ $image['imageUrl'] }}">
        <img src="{{ $image['imageUrl'] }}"/>
      </a>
    @endforeach
  </div>
  @endif
  <hr class="fadebar"/>
  <div class="shareblock">
    <div class="share">Share:</div>
    <ul>
      <li class="facebookicon"/>
    </ul>
    <div>Direct Link:
      <input type="text" id="storyurl" readonly="readonly" value="{{ $storyData['storyUrl'] }}"/>
    </div>
  </div>
  <hr class="fadebar"/>
  <div class="fb-comments" data-width="100%" data-href="{{ $storyData['storyUrl'] }}" data-numposts="5" data-colorscheme="light"/>
</div>

<script type="text/javascript">
$(function () {
    $.ajaxSetup({ cache: true });
    $.getScript('//connect.facebook.net/en_UK/all.js', function() {
        FB.init({
            appId: '{{ Config::get('anvard::hybridauth')['providers']['Facebook']['keys']['id'] }}',
        });
        FB.XFBML.parse();
    });

    if ($('div#imagegalleryblock').length) {
        Galleria.loadTheme('/resources/galleria/galleria.classic.js');
        Galleria.run('#imagegalleryblock');
    }
});

$('li.facebookicon').click(function () {
    FB.ui({
        method: 'feed',
        display: 'dialog',
        link: '{{ $storyData['storyUrl'] }}',
        name: '{{ $storyData['story']->title or '' }}',
        caption: '{{ $storyData['storySummary'] or '' }}'
    }, function (response) {
        if (response && !response.error_code) {
            popupDialog('Success', 'Thank you for sharing your story on Facebook!');
        }
    });
});
$('input#storyurl').click(function () {
    $(this).select();
});

</script>

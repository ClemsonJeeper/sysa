@extends('layouts.master')

@section('header')
{{ HTML::script('/resources/js/redactor/redactor.min.js') }}
{{ HTML::script('/resources/js/redactor/fontcolor.js') }}
{{ HTML::script('/resources/js/redactor/video.js') }}
{{ HTML::script('/resources/js/dropzone.min.js') }}
{{ HTML::style('/resources/css/redactor.css') }}
@stop

@section('content')
<div id="bodyblock">
@if (!$data['editMode'])
  @include('layouts.story.progress')
@endif
  {{ $data['progress'] or '' }}
  @if ($data['editMode'])
    <h2>Edit Your Story</h2>
    <small>By editing your story, it will revert back to being 'unpublished' and have to be re-approved.</small><br/>
  @else
    <h2>Share Your Story</h2>
    <small>
      <p>It's time to put your story together!</p>
      <p>You can use text, photos, and video - as much or as little as you like.</p>
    </small>
  @endif
  <div class="well">
    <div class="container-fluid pull-right">
      <div style="color: red; font-size: 11px">* Required information</div>
    </div>
    {{ Form::open(array('url' => $data['formUrl'], 'id' => 'createform')) }}
    {{ Form::hidden('storyId', $data['formData']['storyId']) }}
    @if ($data['editMode'])
      {{ Form::hidden('editMode', true) }}
    @endif
    <div class="form-group required">
      <label class="control-label" for="name">First Name</label>
      {{ Form::text('name', $data['formData']['storyFirstName'], array('placeholder' => '<First Name>', 'id' => 'name', 'class' => 'form-control')) }}
    </div>
    <div class="form-group required">
      <label class="control-label" for="title">Title</label>
      {{ Form::text('title', $data['formData']['storyTitle'], array('placeholder' => '<Give your story a title>', 'id' => 'title', 'class' => 'form-control')) }}
    </div>
    <div class="form-group required">
      <label class="control-label" for="storyText">Story Text</label>
      <div class="form-control" style="height: 350px">
      {{ Form::textarea('storyText', $data['formData']['storyHtml'], array('id' => 'storytext', 'class' => 'form-control')) }}
      </div>
    </div>
    <div class="form-group">
      <label class="control-label" for="images">Photos</label>
      <div style="padding: 5px 0">Either click below to choose an image to upload, or drag an image from your computer into the box to upload it.</div>
      <div name="images" class="form-control" style="height: 300px" id="photoblock"/>
    </div>
    {{ Form::close() }}
  </div>
  <div class="row">
    <div class="container">
      <div class="col-md-6 text-left">
        @if (!$data['editMode'])
          <button type="button" class="btn btn-primary back">Go Back</button>
        @endif
      </div>
      <div class="col-md-6 text-right">
        <button type="button" class="btn btn-primary save">@if ($data['editMode']) Save Changes @else Preview and Share @endif</button>
      </div>
    </div>
  </div>
</div>

<div id="template" style="display: none">
  <div class="dz-preview dz-file-preview" id="preview">
    <div class="dz-details">
      <div class="dz-filename"><span data-dz-name></span></div>
      <div class="dz-size" data-dz-size></div>
      <img data-dz-thumbnail />
    </div>
    <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
    <div class="favorite-mark"><span></span></div>
    <div class="dz-error-mark"><span>✘</span></div>
    <div class="dz-button-box">
      <span class="glyphicon glyphicon-heart text-grey dz-favorite btn-lg"></span>
      <span class="glyphicon glyphicon-remove text-red dz-favorite btn-lg"></span>
    </div>
  </div>
</div>

<script type="text/javascript">
$('#storytext').redactor({
    minHeight: 290,
    maxHeight: 290,
    plugins: [ 'fontcolor', 'video' ]
});

$('div#buttonblock div#leftbutton').click(function () {
    window.location.href = '/story/topic';
});

$('button.save').click(function () {
    if ($('input[name=name]').val().length == 0) {
        popupDialog('Error', 'Please enter your first name.', function () {
            $('input[name=name]').focus();
        });
        return;
    }
    if ($('input[name=title]').val().length == 0) {
        popupDialog('Error', 'Please enter a title for your story.', function () {
            $('input[name=title]').focus();
        });
        return;
    }
    if ($.trim($('#storytext').redactor('clean.stripTags', $('#storytext').redactor('code.get'))).length == 0) {
        popupDialog('Error', 'Please tell us your story.', function() {
            $('#storytext').redactor('focus.setStart');
        });
        return;
    }

    $('#createform').submit();
});

@if (!$data['editMode'])
$('button.back').click(function () {
    window.location.href = '{{ Config::get('app.url') . '/story/topic' }}';
});
@endif


$(function () {
    var favorite = false;

    var dz = new Dropzone('div#photoblock', {
        url: '/story/imageUpload',
        paramName: 'image',
        maxFiles: 4,
        previewContainer: '#photoblock',
        previewTemplate: $('div#preview').parent().html(),
        init: function () {
            var myDropzone = this;

            // Called when generating a thumbnail
            this.on('thumbnail', function (file, result) {
                if (favorite) {
                    $(file.previewElement).find('div.dz-button-box span.glyphicon-heart').removeClass('text-grey').addClass('text-red');
                }
            });

            // Called before sending image over, set favorite status
            this.on('sending', function (file, xhr, formData) {
                formData.append('storyId', $('input[name=storyId]').val());
            });

            // Called after file was added, add the remove/favorite handlers
            this.on('addedfile', function (file) {
                $(file.previewElement).find('.glyphicon-remove').click(function () {
                    return dz.removeFile(file);
                });
                $(file.previewElement).find('.glyphicon-heart').click(function () {
                    if ($(this).hasClass('text-grey')) {
                        var favoriteImage = this;
                        $.ajax('{{ Config::get('app.url') . '/story/imageFavorite' }}', {
                            data: {
                                imageId: file.imageId
                            },
                            success: function (data, textStatus, jqXHR) {
                                if (data && data.success) {
                                    $('div#photoblock .text-red.glyphicon-heart').each(function () {
                                        $(this).removeClass('text-red').addClass('text-grey');
                                    });
                                    $(favoriteImage).removeClass('text-grey').addClass('text-red');
                                }
                            }
                        });
                    }
                });
            });

            // Called after file removed, tell our app to remove from db
            this.on('removedfile', function (file) {
                $.ajax('/story/imageRemove', {
                    data: {
                        imageId: file.imageId
                    },
                    success: function (data, textStatus, jqXHR) {
                    }
                });
            });

            // Called when upload is complete, remove the progress bar
            this.on('complete', function (file) {
                $(file.previewElement).find('.dz-progress').remove();
            });

            // Called when upload is successful, set the imageId locally
            this.on('success', function (file, responseText, e) {
                file.imageId = responseText.imageId;
                if (responseText && responseText.favorite) {
                    $(file.previewElement).find('div.dz-button-box span.glyphicon-heart').removeClass('text-grey').addClass('text-red');
                }
            });

            // Called when there is an error, pop up a dialog error
            this.on('error', function (file, message, xhr) {
                dz.removeFile(file);
                popupDialog('Image Upload Error', message);
            });

            var mockImage = {};
            @if (isset($data['formData']['storyImages']))
            @foreach ($data['formData']['storyImages'] as $image)
                mockImage = { name: 'x.png', size: 1, imageId: {{ $image->id }}, accepted: true };
                myDropzone.emit('addedfile', mockImage);
                @if ($image->favorite)
                    favorite = true;
                @else
                    favorite = false;
                @endif
                myDropzone.emit('thumbnail', mockImage, '{{ $image->getThumbUrl() }}');
                myDropzone.files.push(mockImage);
            @endforeach
            @endif
        }
    });
    $('#template').remove();
});

</script>

@stop

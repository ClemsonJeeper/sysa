@extends('layouts.master')

@section('content')
@include('layouts.story.progress')
<div id="bodyblock">
  <div class="stepcontent"><img src="/resources/images/1.big.png"/><div class="steptext">Sign Up or Log In</div></div>
  <br/>
  <div class="container-fluid">
    <strong><p>Ready to share your story?</p></strong><br/>
    <strong>Here's how it works</strong>
    <div style="font-size: 12px">
      1. Read and accept the Terms and conditions below.<br/>
      2. Create an account by either logging in with your social network or by entering your email and creating a password.</br/>
      3. Create your story using our Story Wizard, with optional images or video.
    </div><br/>
    <strong>Get the word out</strong>
    <div style="font-size: 12px">After you create your story, you can share your story on our site as well as your social media sites.</div><br/><br/>
    <hr class="fadebar"/>
    <h3><strong>Sign Up or Log In to Share Your Story!</strong></h3>
    <br/>
    <div class="container-fluid">
      <ul class="nav nav-pills">
        <li role="presentation" @if ($data['page'] == 'login') class="active" @endif><a href="{{ Config::get('app.url') . '/story/login?page=login' }}">Log In</a></li>
        <li role="presentation" @if ($data['page'] == 'signup') class="active" @endif><a href="{{ Config::get('app.url') . '/story/login?page=signup' }}">Sign Up</a></li>
      </ul>
      <div class="well">
        <div class="row">
          <div class="col-md-6">
            @if ($data['page'] == 'login')
              <h5>Log in via your social media account:</h5>
            @else
              <h5>Sign up using your social media account:</h5>
            @endif
            @if (Config::get('anvard::hybridauth.providers')['Twitter']['enabled'])
              <div class="socialblock" data-social="twitter">
                <img src="{{ Config::get('app.url') . '/resources/images/twitter.png'}}"/>
                <div>Sign @if ($data['page'] == 'login') In @else Up @endif with <b>Twitter</b></div>
              </div>
            @endif
            @if (Config::get('anvard::hybridauth.providers')['Facebook']['enabled'])
              <div class="socialblock" data-social="facebook">
                <img src="{{ Config::get('app.url') . '/resources/images/facebook.png'}}"/>
                <div>Sign @if ($data['page'] == 'login') In @else Up @endif with <b>Facebook</b></div>
              </div>
            @endif
            @if (Config::get('anvard::hybridauth.providers')['Google']['enabled'])
              <div class="socialblock" data-social="google">
                <img src="{{ Config::get('app.url') . '/resources/images/googleplus.png'}}"/>
                <div>Sign @if ($data['page'] == 'login') In @else Up @endif with <b>Google+</b></div>
              </div>
            @endif
            <small>Don't worry, we'll never post without your permission!</small>
        <br/><br/><br/>
          </div>
          <div class="col-md-6" style="border-left: 1px solid #ccc">
            @if ($data['page'] == 'login')
              <h5>Log in using your existing "Share Your Story" account:</h5>
            @else
              <h5>Sign up using your Email address:</h5>
            @endif
            <div class="form-group">
              <label for="email">Email Address</label>
              <input type="email" class="form-control" id="email" placeholder="Enter Email address"/>
            </div>
            <div class="form-group">
              <label for="password">Password</label>
              <input type="password" class="form-control" id="password" placeholder="Enter your password"/>
            </div>
            @if ($data['page'] == 'signup')
              <div class="form-group">
                <label for="confirmPassword">Confirm Password</label>
                <input type="password" class="form-control" id="confirmPassword" placeholder="Confirm your password"/>
              </div>
            @endif
            <button id="login" class="btn btn-primary">@if ($data['page'] == 'login') Log In @else Sign Up @endif</button>
            <button id="forgotPassword" class="btn btn-default pull-right">Forgot Password?</button>
          </div>
      </div>
    </div>
    @if ($data['page'] == 'signup')
      <br/>
      <h5><strong>Terms and Conditions of Use and Privacy Policy</strong></h5>
      <div id="termsofuse">
        <div id="termshtml">
          <b>Terms of Use</b>
          {{ $data['termsOfUse'] }}
          <b>Privacy Policy</b>
          {{ $data['privacyPolicy'] }}
        </div>
        <div id="termsofuseconfirm">
          <input type="checkbox" id="termsconfirm"></input>
           I agree to the terms and conditons of use and privacy policy
        </div>
      </div>
    @endif
  </div>
</div>

<div id="forgotpassworddialog" title="Forgot Password" style="display: none">
  <h6>Please enter the Email address you used to create your account and we will send you instructions on how to reset your password to this address.</h6>
  <div class="form-group">
    <label for="email">Email Address</label>
    {{ Form::text('email', '', array('class' => 'form-control', 'placeholder' => 'Enter Email address', 'id' => 'forgotEmail')) }}
  </div>
</div>

<script type="text/javascript">

$('button#login').click(function () {
    $.ajax('{{ Config::get('app.url') . '/login/login' }}', {
        data: {
            create: @if ($data['page'] == 'signup') true @else false @endif ,
            email: $('input#email').val(),
            password: $('input#password').val(),
            confirmPassword: $('input#confirmPassword').val(),
            termsConfirm: $('#termsconfirm').is(':checked') ? true : false
        },
        success: function (data, textStatus, jqXHR) {
          console.log(data);
            if (!data.success) {
                popupDialog(data.title, data.message, function () {
                    $('input#' + data.field).focus()
                });
            } else {
                if (data.create) {
                    // User was created successfully,
                    popupDialog(data.title, data.message, function () {
                        window.location.href = '{{ Config::get('app.url') . '/story/login' }}';
                    });
                } else {
                    window.location.href = '{{ Config::get('app.url') . '/story/topic' }}';
                }
            }
        }
    });
});

$('button#forgotPassword').click(function () {
    $('#forgotpassworddialog').dialog({
        width: 500,
        buttons: {
            'Send Password': function () {
                var thisDialog = this;
                if (!$('input#forgotEmail').val().trim().length) {
                    popupDialog('Error', 'You must enter your Email address to reset your password.');
                    return;
                }    
                $.ajax('{{ Config::get('app.url') . '/password/remind' }}', {
                    type: 'post',
                    data: {
                        email: $('input#forgotEmail').val()
                    },
                    success: function (data, textStatus, jqXHR) {
                        if (data.success) {
                            popupDialog('Success', data.message, function () {
                                $(thisDialog).dialog('close');
                            });
                        } else {
                            popupDialog('Error', data.message);
                        }
                    }
                });
            }
        }
    });
});

$('div.socialblock').click(function () {
    window.location.href = '{{ Config::get('app.url') . '/login/' }}' + $(this).attr('data-social');
});

$('#email').focus();

</script>
@stop

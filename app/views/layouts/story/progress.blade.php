<div id="progressblock">
  <ul>
    @if ($data['step'] == 1)
    <li class="current">
    <img src="/resources/images/1.big.png"/>
    @else
    <li>
    <img src="/resources/images/1.png"/>
    @endif
    <div>Sign Up</div></li>
    @if ($data['step'] == 2)
    <li class="current">
    <img src="/resources/images/2.big.png"/>
    @else 
    <li>
    <img src="/resources/images/2.png"/>
    @endif
    <div>Choose a Topic</div></li>
    @if ($data['step'] == 3)
    <li class="current">
    <img src="/resources/images/3.big.png"/>
    @else 
    <li>
    <img src="/resources/images/3.png"/>
    @endif
    <div>Create Your Story</div></li>
    @if ($data['step'] == 4)
    <li class="current">
    <img src="/resources/images/4.big.png"/>
    @else 
    <li>
    <img src="/resources/images/4.png"/>
    @endif
    <div>Share</div></li>
  </ul>
</div>

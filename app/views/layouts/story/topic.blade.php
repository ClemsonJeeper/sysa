@extends('layouts.master')

@section('content')
@include('layouts.story.progress')
<div id="bodyblock">
  <div class="stepcontent"><img src="{{ Config::get('app.url') . '/resources/images/2.big.png' }}"/><div class="steptext">Choose a Topic</div></div>
  <br/>
  <div class="container-fluid">
    <p>Choose a topic that <b>best fits</b> what you want to talk about.</p>
    <p>This helps us share your story with people looking for what you have to say!</p>
    <br/>
    {{ Form::open(array('url' => Config::get('app.url') . '/story/create', 'id' => 'topicform')) }}
    <table class="table table-bordered">
      <tr>
        <th></th>
        <th class="col-sm-7"></th>
        <th class="col-sm-5 text-right"></th>
      </tr>
      @foreach ($data['topicsData'] as $topic)
        <tr>
          <td>
            @if ($topic['selected'])
              {{ Form::radio('topicId', $topic['id'], true) }}
            @else
              {{ Form::radio('topicId', $topic['id']) }}
            @endif
          </td>
          <td>
            <strong>{{ $topic['name'] }}</strong><br/>
            <small>{{ $topic['short_description'] }}</small>
          </td>
          <td>{{ $topic['long_description'] }}</td>
        </tr>
      @endforeach
    </table>
    {{ Form::close() }}
    <button id="submitbutton" class="btn btn-primary pull-right" style="width: 100px">Next</button>
  </div>
</div>

<script type="text/javascript">
$(function() {
    $('button#submitbutton').click(function () {
        if ($('input[name=topicId]:checked').val() === undefined) {
            popupDialog('Error', 'You must choose a topic.');
            return;
        }
        $('#topicform').submit();
    });
});
</script>
@stop

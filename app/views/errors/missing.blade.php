<!DOCTYPE html>
<html>
  <head>
    {{ HTML::style(Config::get('app.url') . '/resources/css/bootstrap.min.css') }}
    {{ HTML::style(Config::get('app.url') . '/resources/css/bootstrap-theme.min.css') }}
  </head>
  <body>
    <div class="container alert alert-danger" style="margin-top: 50px; padding: 50px">
        <h1>Uh oh!</h1>
        <p>You've reached a page that doesn't exist or there has been an internal error.  Click <a href="{{ Config::get('app.url') . '/embed/splash' }}">here</a> to return back to the main 'Share Your Story' landing page.</p>
     </div>
  </body>
</html>

<?php

class Shortener
{
    // Characters to use in short code.  No vowels (so no accidental bad words).
    protected static $chars = "123456789bcdfghjkmnpqrstvwxyzBCDFGHJKLMNPQRSTVWXYZ";

    /**
     * Converts an integer id in the database to a unique shortCode.  This
     * should be good for several billion stories (given a max-length 8 shortCode)
     *
     * @param $id integer Id to generate a shortCode for
     * @return string shortCode for the given Id
     */
    static public function convertIntToShortCode ($id)
    {
        $id = intval($id);
        if ($id < 1) {
            throw new Exception('The ID is not a valid integer');
        }

        $length = strlen(self::$chars);
        $code = '';
        while ($id > $length - 1) {
            // determine the value of the next higher character
            // in the short code should be and prepend
            $code = self::$chars[intval(fmod($id, $length))] . $code;
            // reset $id to remaining value to be converted
            $id = floor($id / $length);
        }

        // remaining value of $id is less than the length of
        // self::$chars
        $code = self::$chars[intval($id)] . $code;

        return $code;
    }

    /**
     * Return the story's full short url given the shortCode
     *
     * @param $shortCode string Short code to generate URL for
     * @return string Full URL of story
     */
    static public function getShortUrl ($shortCode)
    {
        return Config::get('app.url') . '/story/show/' . $shortCode;
    }

    /**
     * Validate that a short code passed in doesn't contain any illegal
     * characters.
     *
     * @param $shortCode string Short code to analyze
     * @return bool Short code is valid or not
     */
    static public function validateShortCode ($shortCode)
    {
        for ($i = 0; $i < strlen($shortCode); $i++) {
            if (strpos(self::$chars, $shortCode[$i]) === false) {
                return false;
            }
        }
        return true;
    }
}

?>

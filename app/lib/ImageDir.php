<?php

/**
 * This class provides an interface for storing a massive amount of images on
 ud the server.  It uses the database to track the directory structure and only
 * allows 10,000 files per directory before increasing and bumping to the next
 * location.
 */
class ImageDir
{
    /* The number of images per imagedir before creating a new one */
    const MAX_PER_IMAGEDIR = 10000;

    /**
     * Returns the full Url to the given imageName
     *
     * @param $imageName string Image Name
     * @return string Full Url to the given image
     */
    static public function getImageUrl ($imageName)
    {
        return Config::get('app.url') . '/resources/images/story/' . $imageName;
    }

    /**
     * Returns the full Url to the given thumbnail
     *
     * @param $imageName string Image Name
     * @return string Full Url to the given image thumbnail
     */
    static public function getImageThumbUrl ($imageName)
    {
        $thumbName = pathinfo($imageName, PATHINFO_DIRNAME) . '/' .
            pathinfo($imageName, PATHINFO_FILENAME) . '_thumb.' .
            pathinfo($imageName, PATHINFO_EXTENSION);

        return Config::get('app.url') . '/resources/images/story/' . $thumbName;
    }

    /**
     * Generates a unique basename of an image in a directory
     *
     * @param $dir string Directory to generate file name in
     * @param $extension string Extension of file to generate
     * @return string unique base name of file
     */
    static private function generateUniqueBaseName ($dir, $extension)
    {
        $ok = false;

        if (!file_exists($dir)) {
            return null;
        }

        while (!$ok) {
            $rand1 = rand(1, 99999999);
            $rand2 = rand(1, 99999999);
            $baseName = sprintf('%08d', $rand1) . '_' . sprintf('%08d', $rand2);
            $filename = $baseName . '.' . $extension;

            if (!file_exists($filename)) {
                return $baseName;
            }
        }
    }

    /**
     * Store a Input::file (known good image) into our imageDir filesystem.
     *
     * @param $image Input::file Image to move to our imageDir filesystem
     * @return string NULL if unsuccessful, or filename to be used by image if
     *   successful
     */
    static public function storeImage ($image)
    {
        $settings = Setting::first();
        $imageDir = public_path() . '/resources/images/story/';

        /*
         * Note there is a possibility we could have a few over 10k because
         * we're not locking the database during this, but not a huge deal.
         */
        $current = $settings->imagedir_current;
        $count = $settings->imagedir_count;

        $count++;
        if ($count > self::MAX_PER_IMAGEDIR) {
            $count = 0;
            $current++;
        }

        $imageDir .= $current;

        if (!file_exists($imageDir)) {
            if (!mkdir($imageDir, 0777)) {
                return NULL;
            }
            chmod($imageDir, 0777);
        }

        DB::table('settings')->update(array(
            'imagedir_current' => $current,
            'imagedir_count' => $count,
        ));

        $baseName = ImageDir::generateUniqueBaseName($imageDir, $image->getClientOriginalExtension());
        $name = $baseName . '.' . $image->getClientOriginalExtension();
        $image->move($imageDir, $name);

        /*
         * Now create a thumbnail for said image
         */
        $interventionImage = IntImage::make($imageDir . '/' . $name);
        $interventionImage->resize(200, null, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });
        $interventionImage->save($imageDir . '/' . $baseName . '_thumb.' . $image->getClientOriginalExtension(), 75);

        return $current . '/' . $name;
    }

    /**
     * Remove an image from the database & filesystem (note: checks 
     * permissions).
     *
     * @param $imageId integer Image id to remove
     * @return null
     */
    static public function removeImage ($imageId)
    {
        $imageDir = public_path() . '/resources/images/story/';

        if (!$imageId) {
            return '';
        }

        $image = Image::find($imageId);
        if (!$image) {
            return '';
        }

        if ($image->user_id != Auth::id()) {
            return '';
        }


        // First remove the actual image
        @unlink($imageDir . $image->filename);

        // Now the thumbnail
        $thumbName = pathinfo($image->filename, PATHINFO_DIRNAME) . '/' .
            pathinfo($image->filename, PATHINFO_FILENAME) . '_thumb.' .
            pathinfo($image->filename, PATHINFO_EXTENSION);

        @unlink($imageDir . $thumbName);

        $storyId = $image->story_id;

        // Now nuke from database
        $image->delete();

        // Make sure there is still a favorite for this story
        $images = Image::where('story_id', '=', $storyId)
            ->where('favorite', '=', '1')
            ->get();

        if ($images->count() == 0) {
            // Nope, set the first one to favorite
            $image = Image::where('story_id', '=', $storyId)->first();
            if ($image && $image->count() == 1) {
                $image->favorite = 1;
                $image->save();
            }
        }

        return '';
    }

    /** 
     * Favorite an image out of a story's image.
     *
     * @param $imageId int Image ID to set favorite
     * @return bool Success?
     */
    static public function favoriteImage ($imageId)
    {
        // No access
        if (!Auth::check()) {
            return false;
        }

        $image = Image::find($imageId);
        if (!$image || ($image->user_id != Auth::id())) {
            return false;
        }

        DB::table('images')
            ->where('story_id', $image->story_id)
            ->where('user_id', $image->user_id)
            ->update(array('favorite' => 0));

        $image->favorite = 1;
        $image->save();

        return true;
    }
}

?>

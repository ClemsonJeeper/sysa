<?php

class Utils
{
    const MIN_PASSWORD_LENGTH = 8;
    const MAX_PASSWORD_LENGTH = 64;

    /**
     * Gets a plain text summary from an Html blurb
     *
     * @param $html string Html to get plain text from
     * @param $numchars integer Max # of characters to return
     * @return string The plain text summary
     */
    static private function getPlainTextSummaryFromHTML ($html, $numchars)
    {
        $html = strip_tags($html);
        $html = html_entity_decode($html, ENT_QUOTES, 'UTF-8');
        $html = mb_substr($html, 0, $numchars, 'UTF-8');
        $html .= "…";
        return $html;
    }

    /**
     * Returns an array of storyData that can be passed into a story view to
     * render it.
     *
     * @param $story Story The story to parse
     * @return array of information for the story view
     */
    static public function buildStoryData ($story)
    {
        $topic = Topic::where('id', '=', $story->topic_id);
        $topicText = '';
        if ($topic) {
            $topicText = $topic->first()->name . ': '
                . $topic->first()->short_description;
        }

        // If user registered using social media, link to profile photo
        $profilePhotoUrl = '';
        if ($story->user->profiles->count()) {
            $profilePhotoUrl = $story->user->profiles->first()->photoURL;
        }

        $createdAt = new DateTime($story->created_at);

        /*
         * Get our images
         */
        $images = Image::where('story_id', '=', $story->id)
            ->orderBy('favorite', 'DESC')->get();

        $imageData = array();
        $images->each(function ($image) use (&$imageData) {
            $imageData[] = array(
                'imageUrl' => ImageDir::getImageUrl($image->filename),
                'imageThumbUrl' =>
                    ImageDir::getImageThumbUrl($image->filename),
                'favorite' => $image->favorite ? true : false,
            );
        });

        $out = array(
            'story' => $story,
            'storyId' => $story->id,
            'createdAt' => $createdAt->format('l jS F Y \a\t g:ia'),
            'storyUrl' => Shortener::getShortUrl($story->short_code),
            'storySummary' =>
                addslashes(Utils::getPlainTextSummaryFromHTML($story->story_html,
                    120)),
            'storyTopic' => $topicText,
            'imageData' => $imageData,
        );

        if (strlen($profilePhotoUrl)) {
            $out['profilePhotoUrl'] = $profilePhotoUrl;
        }

        return $out;
    }

    /**
     * Returns our navbar arrays
     * @param $topicId int Which topic is highlighted (-1 for Dashboard, 0 for
     *   Home)
     * @return array Array of navbar data to be used by template
     */
    static public function getNavbarData ($topicId)
    {
        $topics = Topic::all()->sortBy('order');

        $navbarData['left'] = array(
            array(
                'name' => 'Home',
                'url' => Config::get('app.url') . '/embed/splash',
                'selected' => ($topicId == 0),
            )
        );
        foreach ($topics as $topic) {
            $navbarData['left'][] = array(
                'name' => $topic->name,
                'url' => Config::get('app.url') . '/embed/topic/' . $topic->id,
                'selected' => ($topicId == $topic->id),
            );
        }

        if (Auth::check()) {
            $navbarData['right'] = array(
                array(
                    'name' => 'Dashboard',
                    'url' => Config::get('app.url') . '/embed/dashboard',
                    'selected' => ($topicId == -1),
                ),
                array(
                    'name' => 'Logout',
                    'url' => Config::get('app.url') . '/embed/splash?logout=true',
                    'selected' => false,
                )
            );
            if (Auth::user()->admin) {
                array_unshift($navbarData['right'], array(
                    'name' => 'Admin',
                    'url' => Config::get('app.url') . '/admin/dashboard',
                    'selected' => ($topicId == -3),
                ));
            }
        } else {
            $navbarData['right'] = array(
                array(
                    'name' => 'Login',
                    'url' => Config::get('app.url') . '/story/login',
                    'selected' => false,
                )
            );
        }

        return $navbarData;
    }

    /**
     * Returns our navbar arrays
     * @param $current int Which admin page are we on
     * @return array Array of admin navbar data to be used by template
     */
    static public function getAdminNavbarData ($current)
    {
        $url = Config::get('app.url') . '/admin/';

        $approvedString = '';
        $stories = Story::where('state', 'unpublished')
            ->get();
        if ($stories->count() > 0) {
            $approvedString = ' (' . $stories->count() . ')';
        }
        return array(
            array(
                'name' => 'Dashboard',
                'url' => $url . 'dashboard',
                'selected' => ($current == AdminController::PAGE_DASHBOARD),
            ),
            array(
                'name' => 'Stories' . $approvedString,
                'url' => $url . 'stories',
                'selected' => ($current == AdminController::PAGE_STORIES),
            ),
            array(
                'name' => 'Topics',
                'url' => $url . 'topics',
                'selected' => ($current == AdminController::PAGE_TOPICS),
            ),
            array(
                'name' => 'Users',
                'url' => $url . 'users',
                'selected' => ($current == AdminController::PAGE_USERS),
            ),
            array(
                'name' => 'Settings',
                'url' => $url . 'settings',
                'selected' => ($current == AdminController::PAGE_SETTINGS),
            ),
        );
    }
    
    /**
     * Function to check the validity of a password
     *
     * @param $password string Password to check
     * @param $confirmPassword string Confirmation password to check
     * @return array Array of information if password valid or not and if not
     *   why not
     */
    static public function validatePassword ($password, $confirmPassword)
    {
        $success = true;
        $title = $message = $field = '';

        if (strlen($password) < Utils::MIN_PASSWORD_LENGTH ||
            strlen($password) > Utils::MAX_PASSWORD_LENGTH)
        {
            $success = false;
            $title = 'Invalid Password';
            $message = 'Please enter a secure password of at least ' .
                Utils::MIN_PASSWORD_LENGTH . ' characters.';
            $field = 'password';
        } else if (empty($confirmPassword)) {
            $success = false;
            $title = 'Invalid Password';
            $message = 'Please confirm your password.';
            $field = 'confirmPassword';
        } else if ($password != $confirmPassword) {
            $success = false;
            $title = 'Invalid Password';
            $message = 'Password and confirmation do not match.  Please try again.';
            $field = 'password';
            // XXX RKJ ADD 'smart' PASSWORD REQUIREMENTS
        }

        return array(
            'success' => $success,
            'title' => $title,
            'message' => $message,
            'field' => $field,
        );
    }

}

?>

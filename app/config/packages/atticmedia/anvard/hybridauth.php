<?php

return array(
    'providers' => array(
        'Google' => array (
            'enabled' => false,
            'keys' => array(
                'id' => '',
                'secret' => '',
            ),
        ),
        'Facebook' => array(
            'enabled' => false,
            'keys' => array(
                'id' => '',
                'secret' => '',
            ),
            'scope' => 'email, publish_actions',
        ),
        'Twitter' => array (
            'enabled' => false,
            'keys' => array(
                'key' => '',
                'secret' => '',
            ),
        ),
    ),
);

<?php
return array(
    'index' => 'anvard',
    'login' => 'anvard/login/{provider}',
    'loginredirect' => 'story/create',
    'logout' => 'anvard/logout',
    'logoutredirect' => 'story/create',
    'authfailed' => 'story/login',
    'endpoint' => 'anvard/endpoint',
);

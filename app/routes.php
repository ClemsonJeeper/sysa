<?php

App::missing(function($exception)
{
    return Response::view('errors.missing', array(), 404);
});

/*
 * Index
 */
Route::get('/', function () { return Redirect::to('story/login'); });

/*
 * Story Controller
 */
Route::get('story/login', 'StoryController@login');
Route::get('story/topic', 'StoryController@topic');
Route::post('story/create', 'StoryController@create');
Route::post('story/share', 'StoryController@share');
Route::get('story/show/{id}', 'StoryController@show');
Route::post('story/imageUpload', 'StoryController@imageUpload');
Route::get('story/imageRemove', 'StoryController@imageRemove');
Route::get('story/imageFavorite', 'StoryController@imageFavorite');
Route::get('story/edit', 'StoryController@edit');
Route::post('story/edit', 'StoryController@edit');


/*
 * Login Controller
 */
Route::get('login/login', 'LoginController@login');
Route::get('login/logout', 'LoginController@logout');
Route::get('login/facebook', 'LoginController@facebookLogin');
Route::get('login/twitter', 'LoginController@twitterLogin');
Route::get('login/google', 'LoginController@googleLogin');
Route::get('login/validate', 'LoginController@validate');
Route::get('login/changeEmail', 'LoginController@changeEmail');
Route::get('login/validateEmailChange', 'LoginController@validateEmailChange');
Route::get('login/changePassword', 'LoginController@changePassword');

/*
 * Embed Controller
 */
Route::get('embed/splash', 'EmbedController@splash');
Route::get('embed/topic/{topicId}', 'EmbedController@topic');
Route::get('embed/rawstories', 'EmbedController@rawStories');
Route::get('embed/dashboard', 'EmbedController@dashboard');

/*
 * Admin Controller
 */
Route::get('admin/dashboard', 'AdminController@dashboard');
Route::get('admin/stories', 'AdminController@stories');
Route::get('admin/topics', 'AdminController@topics');
Route::post('admin/topics', 'AdminController@topics');
Route::get('admin/users', 'AdminController@users');
Route::get('admin/settings', 'AdminController@settings');
Route::post('admin/settings', 'AdminController@settings');
Route::get('admin/create', 'AdminController@create');

/*
 * Reminders Controller
 */
Route::controller('password', 'RemindersController');

?>

# Share Your Story Application

## Installing and Configuring SYSA

AN IMPORTANT NOTE ABOUT APPLICATION SECURITY!!

This app will pass clear-text passwords back and forth between the browser and the application during the log in process.  It is *HIGHLY* recommended that the entire application be secured using HTTPS, otherwise user passwords will be vulnerable to packet inspection.

To configure the application, you will need to update several files:

### app/config/app.php

* Set the "url" to the fully qualified domain name of where SYSA is deployed
* Set the "key" to a random 32 character string

### app/config/database.php

First, you will need to create your database.  You can use the ``scripts/create-database.sql`` script to create the schema of the database. Make note of the database name that you use:

``mysql -u <username> -p <database> < scripts/create-database.sql``

Now, modify ``app/config/database.php`` with:

* Under "connections" specify the proper credentials to your database you have set up

### app/config/mail.php

* Set the "driver", "host", "port", "from", "username", "password" fields appropriately to your mail server so SYSA can send out Emails to your users

### app/config/packages/atticmedia/anvard/hybridauth.php

This file contains the information to tie to your Social Media app accounts.  Please see below for information on configuring this.

### Directory Permissions

The following directories (and everything below them) need to be writable by the web server.

```
app/storage
public/resources/images/story
```

## Configuring Social Networks

In order for SYSA to be deployed properly, you must create the necessary Social Networking applications and associate them inside ``app/config/packages/atticmedia/anvard/hybridauth.php``.

### Facebook

To create a Facebook app, visit https://developers.facebook.com/

* Click Apps -> Add a New App
* Choose Website
* Choose a name for the App (your users will see this when authenticating)
* Skip Quick Start
* Modify ``app/config/packages/atticmedia/anvard/hybridauth.php`` and go to the Facebook entry.  Fill in the "id" with the value from the "App ID" on the Facebook Dev portal.  Fill in the "secret" with the value from the "App Secret" on the Facebook Dev portal.  Be sure to set "enabled" to true for Facebook.
* Select Settings in the Dev portal
* Choose "Add Platform" and choose Website
* Fill in the Full site URL to the SYSA website & Save changes
* Make any other necessary changes you want (descriptions, keywords, etc).
* Submit the app for review

### Twitter

To create a Twitter app, visit https://apps.twitter.com/

* Click "Create New App"
* Choose a name, description, and point the website to the full site URL where SYSA is deployed
* For "Callback URL" just put the same value as the website
* After creating the app, go to "Keys and Access Tokens"
* Modify ``app/config/packages/atticmedia/anvard/hybridauth.php`` and go to the Twitter entry.  Fill in the "key" field with the value from the "Consumer Key (API Key)" from the Twitter Dev portal.  Fill in the "secret" field with the value from the "Consumer Secret (API Secret)" field on the Twitter dev portal.  Be sure to set "enabled" to true for Twitter.

### Google +

To create a Google+ app, visit https://console.developers.google.com

* Click "Create Project"
* Create a project name and unique project ID
* Click the new project name to edit the project
* In the left sidebar, select APIs below "APIs and Auth", find the "Google+ API" service and set it ON.  You can turn off other services you don't need.
* In the sidebar under "APIs and Auth" select "Consent screen", choose an Email address and specify a product name
* In the sidebar under "APIs and Auth" select "Credentials"
* Click "Create a new Client ID"
* Choose "Web Application".  For authorized javascript origins, put the full domain to where SYSA is deployed
* For the Authorized Redirect URI, put your SYSA domain plus
"anvard/endpoint?hauth.done=Google".  For example:

If your sysa domain is: ``www.shareyourstory.gov``, you would put ``http://www.shareyourstory.gov/anvard/endpoint?hauth.done=Google`` in this field

* Click "Update"
* Modify ``app/config/packages/atticmedia/anvard/hybridauth.php`` and go to the Google entry.  Fill in the "id" field with the value from the "Client ID" field from the Google Dev portal.  Fill in the "secret" with the value from the "Client Secret" field from the Google Dev Portal.  Make sure "enabled" is set to true for Google.

## PHP Modules

SYSA requires php5-gd, php5-mcrypt, php5-curl, php5-mysql PHP modules installed and enabled.

## Apache Configuration

Make sure the DocumentRoot points to <installed sysa directory>/public

Also make sure that AllowOverride for this directory is set to All

SYSA requires that mod_rewrite is enabled on the apache server.

## Create Admin Account

When you first install the application and visit it, you will be prompted to create an administrator account.  Be sure to do this before making the application public.  This will allow you to create Topics and approve stories.

## Remote Javascript Invoke

On your current website, you can embed the SYSA splash page anywhere on your page.  Simply include the following snippet and it will replace the HTML element that has the id "sysa-embed" with the splash page.

Note that you will need to replace 'http://sysa-example.com' with the fully qualified domain to your SYSA deployment.

```
<script src="http://sysa-example.com/resources/js/jquery.min.js"></script>
<div id="sysa-embed"/>
<script>function rS(d){$('#sysa-embed').html(d.html);}$.get('http://sysa-example.com/embed/splash?a=1',function(){},'jsonp');</script>
```
